<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Products = ([
            [
                'id'=>'1',
                'category_id' => '1',
                'sub_category_id' => '1',
                'name' => 'Lux white solid Fit Polo',
                'price' => '499',
                'description' => 'Care Instructions: Machine Wash
                Fit Type: regular fit
                60% Cotton and 40% Polyester
                Half sleeve
                Made in India
                Regular fit
                Machine wash',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'2',
                'category_id' => '1',
                'sub_category_id' => '1',
                'name' => 'Adidas white solid Fit Polo',
                'price' => '499',
                'description' => 'Care Instructions: Machine Wash
                Fit Type: regular fit
                60% Cotton and 40% Polyester
                Half sleeve
                Made in India
                Regular fit
                Machine wash',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '3',
                'category_id' => '1',
                'sub_category_id' => '1',
                'name' => 'EYEBOGLER Regular Fit Mens Cotton T-Shirt',
                'price' => '459',
                'description' => 'Care Instructions: Machine Wash
                Fit Type: Regular Fit
                Care Instructions: Machine wash
                Fit Type: regular fit
                Cotton Polyester Blend: 60% Cotton and 40% Polyester
                Regular fit and dimensionally accurate size
                Unique design with excellent durable fabric',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '4',
                'category_id' => '1',
                'sub_category_id' => '1',
                'name' => 'Chromozome Mens Plain Regular Fit T-Shirt',
                'price' => '399',
                'description' => 'Care Instructions: Machine Wash
                Fit Type: Classic Fit
                STYLE : OS 10 CREW NECK TEE. FIT : REGULAR. Chromozome Mens Round Neck tee
                Made from 100% Premium Cotton. All Melanges are made with Cotton Rich Fabric
                Modern fit and Premium Look- This Ribbed crew-neck prevents sagging
                Stock up on your favorite tees which are available in Packs of 3 in fabulous colours at knockout prices',
                'quantity' => '2',
                'status' => 'active'
            ],

            [
                'id'=>'5',
                'category_id' => '1',
                'sub_category_id' => '2',
                'name' => 'Kartloom Mens Cotton Casual fit Shirts',
                'price' => '579',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'6',
                'category_id' => '1',
                'sub_category_id' => '2',
                'name' => 'Mens Regular Fit Shirt',
                'price' => '679',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'7',
                'category_id' => '1',
                'sub_category_id' => '2',
                'name' => 'Blue dove Mens Cotton Casual Shirt Full Sleeves',
                'price' => '479',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'8',
                'category_id' => '1',
                'sub_category_id' => '2',
                'name' => 'Diverse Mens Regular Formal Shirt',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'9',
                'category_id' => '1',
                'sub_category_id' => '3',
                'name' => 'Relaxed Fit Casual Trousers',
                'price' => '529',
                'description' => 'Care Instructions: Machine Wash
                Fit Type: Slim Fit
                Stretch: Stretchable
                Occasion: Casual
                Length: Ankle length
                Fabric: 97% Cotton; 3% Spandex
                Style: Casual Trousers ;Pattern: Solid',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'10',
                'category_id' => '1',
                'sub_category_id' => '3',
                'name' => 'Symbol Mens Relaxed Fit Casual Trousers',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'11',
                'category_id' => '1',
                'sub_category_id' => '3',
                'name' => 'McHenry Mens Regular Fit Formal Trousers',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'12',
                'category_id' => '1',
                'sub_category_id' => '3',
                'name' => ' Regular Casual Trousers',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'13',
                'category_id' => '1',
                'sub_category_id' => '4',
                'name' => ' Cartridges Mens Blue Denim Jeans',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'14',
                'category_id' => '1',
                'sub_category_id' => '4',
                'name' => 'Cherokee Mens Slim Fit Jeans',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'15',
                'category_id' => '1',
                'sub_category_id' => '4',
                'name' => ' Cherokee black full Slim Fit Jeans',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'16',
                'category_id' => '1',
                'sub_category_id' => '4',
                'name' => ' Diverse Mens Relaxed Fit Jeans',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'17',
                'category_id' => '1',
                'sub_category_id' => '5',
                'name' => 'Dixcy Scott Men’s Trunk Snug Fit Solid Innerwear',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'18',
                'category_id' => '1',
                'sub_category_id' => '5',
                'name' => 'Lux Men’s Trendy Fashion Gym Vest',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'19',
                'category_id' => '1',
                'sub_category_id' => '5',
                'name' => 'Trendy Fashion Gym Vest body Fit Solid innerwear',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'20',
                'category_id' => '1',
                'sub_category_id' => '5',
                'name' => ' LUX VENUS Mens Cotton Vest ',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'21',
                'category_id' => '1',
                'sub_category_id' => '6',
                'name' => ' NEVER LOSE Mens Regular Fit Track pants',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'22',
                'category_id' => '1',
                'sub_category_id' => '6',
                'name' => ' NEVER LOSE Trackpants Sports Wear for Men',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'23',
                'category_id' => '1',
                'sub_category_id' => '6',
                'name' => 'Chromozome Mens Lounge Regular Fit Track Pants',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'24',
                'category_id' => '1',
                'sub_category_id' => '6',
                'name' => ' FINZ Mens Slim Fit Track pants',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'25',
                'category_id' => '1',
                'sub_category_id' => '7',
                'name' => 'Pyjama Pants for Sleepwear and Lounge Wear',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'26',
                'category_id' => '1',
                'sub_category_id' => '7',
                'name' => 'Ladies Night Suit',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'27',
                'category_id' => '1',
                'sub_category_id' => '7',
                'name' => ' FUNDAY FASHION Womens Cotton Printed Pyjama Set',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'28',
                'category_id' => '1',
                'sub_category_id' => '7',
                'name' => ' Hemlock Womens Cotton Solid Straight Pyjama',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'29',
                'category_id' => '1',
                'sub_category_id' => '8',
                'name' => ' RUDRSHRI Mens Traditional Ethnic Wear',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'30',
                'category_id' => '1',
                'sub_category_id' => '8',
                'name' => ' ARMAAN ETHNIC Mens Kurta Paired with Pant',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'31',
                'category_id' => '1',
                'sub_category_id' => '8',
                'name' => 'Vastramay Mens Black Cotton Silk',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'32',
                'category_id' => '1',
                'sub_category_id' => '8',
                'name' => 'Men Ethnic & Designer Wear',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'33',
                'category_id' => '1',
                'sub_category_id' => '9',
                'name' => 'Polyester Elastic Knee Compression Bandage',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'34',
                'category_id' => '1',
                'sub_category_id' => '9',
                'name' => 'Compression Sports Socks for Runners',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'35',
                'category_id' => '1',
                'sub_category_id' => '9',
                'name' => 'GLINTO Mens Cotton Socks',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'36',
                'category_id' => '1',
                'sub_category_id' => '9',
                'name' => 'GLINTO Mens Cotton Socks',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'37',
                'category_id' => '1',
                'sub_category_id' => '10',
                'name' => 'MANQ Mens Slim Fit Single Breasted Blazer',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'38',
                'category_id' => '1',
                'sub_category_id' => '10',
                'name' => 'FAVOROSKI Designer Mens Slim',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'39',
                'category_id' => '1',
                'sub_category_id' => '10',
                'name' => 'Casual Business Wedding Suit',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'40',
                'category_id' => '1',
                'sub_category_id' => '10',
                'name' => 'Designer Mens Slim Italian Fit Shawl',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'41',
                'category_id' => '1',
                'sub_category_id' => '11',
                'name' => 'Armisto Mens Warm Woollen Sweater',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'42',
                'category_id' => '1',
                'sub_category_id' => '11',
                'name' => 'Armisto Mens Merino Wool V-Neck Sweater',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'43',
                'category_id' => '1',
                'sub_category_id' => '11',
                'name' => 'Rich Hooded Hoodie Sweatshirt',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'44',
                'category_id' => '1',
                'sub_category_id' => '11',
                'name' => 'Alan Jones Clothing Mens Round Neck Sweater',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'45',
                'category_id' => '1',
                'sub_category_id' => '12',
                'name' => 'VERSATYL Rain Jackets For Men',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'46',
                'category_id' => '1',
                'sub_category_id' => '12',
                'name' => 'Vision VIV Rain Coat for Men’s Water Proof',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'47',
                'category_id' => '1',
                'sub_category_id' => '12',
                'name' => 'rain Jackets for Men',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'48',
                'category_id' => '1',
                'sub_category_id' => '12',
                'name' => 'Full Sleeve Denim Jacket',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'49',
                'category_id' => '1',
                'sub_category_id' => '13',
                'name' => 'Stylish Short Puffed Sleeves',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'50',
                'category_id' => '1',
                'sub_category_id' => '13',
                'name' => 'Stylish Short Puffed Sleeves',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'51',
                'category_id' => '1',
                'sub_category_id' => '13',
                'name' => 'ILLI LONDON Womens TOP',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'52',
                'category_id' => '1',
                'sub_category_id' => '13',
                'name' => 'GLAMCCI Regular wear Kaftan Top for Women',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'53',
                'category_id' => '1',
                'sub_category_id' => '14',
                'name' => 'Cotton Printed Straight Kurti',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'54',
                'category_id' => '1',
                'sub_category_id' => '14',
                'name' => 'Janasya Womens A-Line Crepe Kurta',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'55',
                'category_id' => '1',
                'sub_category_id' => '14',
                'name' => 'Amayra Womens Cotton Anarkali Kurti',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'56',
                'category_id' => '1',
                'sub_category_id' => '14',
                'name' => 'SIRIL Womens Rayon Foil Printed Kurta',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'57',
                'category_id' => '1',
                'sub_category_id' => '15',
                'name' => 'LADUSAA Womens Jacquard Floral',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'58',
                'category_id' => '1',
                'sub_category_id' => '15',
                'name' => 'Rajnandini Womens Salwar Suit Dress Material',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'59',
                'category_id' => '1',
                'sub_category_id' => '15',
                'name' => 'Y2Y Fashion Hot Releases Womens Banarasi',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'60',
                'category_id' => '1',
                'sub_category_id' => '15',
                'name' => 'Khushal K Womens Rayon Printed Kurta',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'61',
                'category_id' => '1',
                'sub_category_id' => '16',
                'name' => 'Salooni Womens Heavy Flower Printed Mysore',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'62',
                'category_id' => '1',
                'sub_category_id' => '16',
                'name' => 'Women Heavy Printed Mysore Silk Saree',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id'=>'63',
                'category_id' => '1',
                'sub_category_id' => '16',
                'name' => 'Salooni Womens Heavy Printed Mysore Silk',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '64',
                'category_id' => '1',
                'sub_category_id' => '16',
                'name' => 'Z-card Foil Print CottonSilk Saree',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '65',
                'category_id' => '1',
                'sub_category_id' => '17',
                'name' => 'minicult Baby Boys Cotton All Over Print',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '66',
                'category_id' => '1',
                'sub_category_id' => '17',
                'name' => 'DThe Boo Boo Club® Kids Cotton Pants, Leggings, Pyjamas, Mix Print - Set of 6',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '67',
                'category_id' => '1',
                'sub_category_id' => '17',
                'name' => 'Chirsh Baby Cotton Pants Lowers Pajama for Kids',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '68',
                'category_id' => '1',
                'sub_category_id' => '17',
                'name' => 'EIO® New-Born Baby Boys and Baby Girls',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '69',
                'category_id' => '2',
                'sub_category_id' => '18',
                'name' => 'ASIAN Mens Wonder-13 Sports Running Shoes',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '70',
                'category_id' => '2',
                'sub_category_id' => '18',
                'name' => 'D Shoes Sports Running Shoes for Mens',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '71',
                'category_id' => '2',
                'sub_category_id' => '18',
                'name' => 'Bourge Mens Loire-z126 Running Shoes',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '72',
                'category_id' => '2',
                'sub_category_id' => '18',
                'name' => 'Bullet-02 Sports,Running,Walking Shoes',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '73',
                'category_id' => '2',
                'sub_category_id' => '19',
                'name' => 'Lee Cooper Mens Lc1473etan Leather Formal Shoes',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '74',
                'category_id' => '2',
                'sub_category_id' => '19',
                'name' => 'Lee Cooper Mens Leather Formal Shoes',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '75',
                'category_id' => '2',
                'sub_category_id' => '19',
                'name' => 'Centrino Men Formal Shoes',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '76',
                'category_id' => '2',
                'sub_category_id' => '19',
                'name' => 'BATA Mens Formal Lace Up Shoes',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '77',
                'category_id' => '2',
                'sub_category_id' => '20',
                'name' => 'Centrino 5631 Casual-Mens Shoes',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '78',
                'category_id' => '2',
                'sub_category_id' => '20',
                'name' => 'Pure Leather Lifestyle Casual Shoes',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '79',
                'category_id' => '2',
                'sub_category_id' => '20',
                'name' => 'Knoos Mens Loafer',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '80',
                'category_id' => '2',
                'sub_category_id' => '20',
                'name' => 'T-Rock Mens Sports Shoes',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '81',
                'category_id' => '2',
                'sub_category_id' => '21',
                'name' => 'Sparx Mens Sd0323g Sneakers',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '82',
                'category_id' => '2',
                'sub_category_id' => '21',
                'name' => 'T-Rock Mens Sneaker',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '83',
                'category_id' => '2',
                'sub_category_id' => '21',
                'name' => 'Klepe Mens Vulcansied PU Sneakers',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '84',
                'category_id' => '2',
                'sub_category_id' => '21',
                'name' => 'parx mens Sc0162g Sneakers',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '85',
                'category_id' => '2',
                'sub_category_id' => '22',
                'name' => 'FENTACIA Men Driving Premium Loafers',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '86',
                'category_id' => '2',
                'sub_category_id' => '22',
                'name' => 'CAT+BLACK Mens Loafers',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '87',
                'category_id' => '2',
                'sub_category_id' => '22',
                'name' => 'Lee Walk Mens Loafers',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '88',
                'category_id' => '2',
                'sub_category_id' => '22',
                'name' => 'VON HUETTE Mens Black/Brown Panny Loafer Shoes',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '89',
                'category_id' => '2',
                'sub_category_id' => '23',
                'name' => 'Flip Flops for Men',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '90',
                'category_id' => '2',
                'sub_category_id' => '23',
                'name' => 'PARAGON Vertex Mens Brown Flip-Flops',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '91',
                'category_id' => '2',
                'sub_category_id' => '23',
                'name' => 'Adidas Mens Adilette Shower Navy Blue Flip-Flops',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '92',
                'category_id' => '2',
                'sub_category_id' => '23',
                'name' => 'Adidas Mens Adirio Attack 2 Ms Slipper',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '93',
                'category_id' => '2',
                'sub_category_id' => '24',
                'name' => 'Big Fox Mens Classic Boot Shoes',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '94',
                'category_id' => '2',
                'sub_category_id' => '24',
                'name' => 'FENTACIA Men Synthetic Casual Boot',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '95',
                'category_id' => '2',
                'sub_category_id' => '24',
                'name' => 'Fentacia Men Synthetic Leather',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '96',
                'category_id' => '2',
                'sub_category_id' => '24',
                'name' => 'Fentacia Men Synthetic Suede Chelsea Boots',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '97',
                'category_id' => '2',
                'sub_category_id' => '25',
                'name' => 'Paragon Mens Grey Formal Sandals',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '98',
                'category_id' => '2',
                'sub_category_id' => '25',
                'name' => 'Centrino Mens Sandals & Floaters',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '99',
                'category_id' => '2',
                'sub_category_id' => '25',
                'name' => 'VIV Mens Outdoor Fisherman Sandals',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '100',
                'category_id' => '2',
                'sub_category_id' => '25',
                'name' => 'Campus Mens Sandals',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '101',
                'category_id' => '2',
                'sub_category_id' => '26',
                'name' => 'Paragon Mens Beige Thong Sandals ',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '102',
                'category_id' => '2',
                'sub_category_id' => '26',
                'name' => 'Red Chief Rust Casual Thong Sandal for Men',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '103',
                'category_id' => '2',
                'sub_category_id' => '26',
                'name' => 'PARAGON Men Brown Flip Flops Thong ',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '104',
                'category_id' => '2',
                'sub_category_id' => '26',
                'name' => 'Paragon Men flip flop',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '105',
                'category_id' => '2',
                'sub_category_id' => '27',
                'name' => 'Red Chief Mens Leather Boat Shoes',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '106',
                'category_id' => '2',
                'sub_category_id' => '27',
                'name' => 'FAUSTO Mens Boat Shoe',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '107',
                'category_id' => '2',
                'sub_category_id' => '27',
                'name' => 'tZaro Genuine Leather Mens Tan Boat',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '108',
                'category_id' => '2',
                'sub_category_id' => '27',
                'name' => 'Ozark by Red Tape Men Boat Shoes',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '109',
                'category_id' => '3',
                'sub_category_id' => '28',
                'name' => 'Charmis Skin Care Combo, Vitamin C Face Serum',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '110',
                'category_id' => '3',
                'sub_category_id' => '28',
                'name' => 'Peeling Solution for Glowing Skin',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '111',
                'category_id' => '3',
                'sub_category_id' => '28',
                'name' => 'Rapid Restore Anti-Ageing Toner',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '112',
                'category_id' => '3',
                'sub_category_id' => '28',
                'name' => 'consists Vitamin C Face Wash brush',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '113',
                'category_id' => '3',
                'sub_category_id' => '29',
                'name' => 'LOréal Professionnel Serie',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '114',
                'category_id' => '3',
                'sub_category_id' => '29',
                'name' => 'Hair Regrowth Oil Controls Hair Fall',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '115',
                'category_id' => '3',
                'sub_category_id' => '29',
                'name' => 'TRESemme Keratin Mask, 300 ml',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '116',
                'category_id' => '3',
                'sub_category_id' => '29',
                'name' => 'Onion Shampoo + Onion Conditioner',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '117',
                'category_id' => '3',
                'sub_category_id' => '30',
                'name' => 'For Nourished & Bright Skin',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '118',
                'category_id' => '3',
                'sub_category_id' => '30',
                'name' => 'Niacinamide Face Serum for Clear',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '119',
                'category_id' => '3',
                'sub_category_id' => '30',
                'name' => 'Good Vibes Corrects Dark Spots',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '120',
                'category_id' => '3',
                'sub_category_id' => '30',
                'name' => 'Boosts Skin Oxygen,Intense skin',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '121',
                'category_id' => '3',
                'sub_category_id' => '31',
                'name' => 'Personal Hygiene Wash For Men & Women',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '122',
                'category_id' => '3',
                'sub_category_id' => '31',
                'name' => 'Nova NHT 1076 Cordless Trimmer',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '123',
                'category_id' => '3',
                'sub_category_id' => '31',
                'name' => 'Gillette Classic Sensitive Shave Foam',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '124',
                'category_id' => '3',
                'sub_category_id' => '31',
                'name' => 'Easy Flossing To Teeth Plaque Tartar',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '125',
                'category_id' => '4',
                'sub_category_id' => '32',
                'name' => 'Metallic Women Watch with One Year Warranty',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '126',
                'category_id' => '4',
                'sub_category_id' => '32',
                'name' => 'SWISSTYLE Analogue Mens Watch',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '127',
                'category_id' => '4',
                'sub_category_id' => '32',
                'name' => 'ORAYAN Full Metallic Red LED Digital Men Watch',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '128',
                'category_id' => '4',
                'sub_category_id' => '32',
                'name' => 'SKYLOFTS Analogue Womens Watch',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '129',
                'category_id' => '4',
                'sub_category_id' => '33',
                'name' => 'NIBOSI Chronograph Mens Watch',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '130',
                'category_id' => '4',
                'sub_category_id' => '33',
                'name' => 'Fossil Chronograph Black Men Watch CH2885',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '131',
                'category_id' => '4',
                'sub_category_id' => '33',
                'name' => 'Solid Leather Belt Watch',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '132',
                'category_id' => '4',
                'sub_category_id' => '33',
                'name' => 'NIBOSI Chronograph Mens Watch',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '133',
                'category_id' => '4',
                'sub_category_id' => '34',
                'name' => 'Helix Mens Analogue Leather Watch',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '134',
                'category_id' => '4',
                'sub_category_id' => '34',
                'name' => 'Decode Analogue Mens Watch',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '135',
                'category_id' => '4',
                'sub_category_id' => '34',
                'name' => 'WRIGHTRACK Analogue Mens Watch',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '136',
                'category_id' => '4',
                'sub_category_id' => '34',
                'name' => 'Casio Enticer Men Analog Black',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '137',
                'category_id' => '4',
                'sub_category_id' => '35',
                'name' => 'Ajanta Analogue Day -Date Stainless Steel',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '138',
                'category_id' => '4',
                'sub_category_id' => '35',
                'name' => 'Relish Mens Black Stainless Steel',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '139',
                'category_id' => '4',
                'sub_category_id' => '35',
                'name' => 'Analog Watch for Men & Women',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '140',
                'category_id' => '4',
                'sub_category_id' => '35',
                'name' => 'Espoir Mens ESP12457 Analog Blue Dial Watch',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '141',
                'category_id' => '4',
                'sub_category_id' => '36',
                'name' => 'Fire-Boltt Ring Bluetooth Calling Smartwatch',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '142',
                'category_id' => '4',
                'sub_category_id' => '36',
                'name' => 'Noise ColorFit Pro 2',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '143',
                'category_id' => '4',
                'sub_category_id' => '36',
                'name' => 'Maxima Max Pro X4 Smartwatch',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '144',
                'category_id' => '4',
                'sub_category_id' => '36',
                'name' => 'realme Smart Watch 2 Pro',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '145',
                'category_id' => '4',
                'sub_category_id' => '37',
                'name' => 'GOQii Vital 3.0 Body Temperature',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '146',
                'category_id' => '4',
                'sub_category_id' => '37',
                'name' => 'OPPO Smart Band with Extra Sport Strap',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '147',
                'category_id' => '4',
                'sub_category_id' => '37',
                'name' => 'MevoFit Bold HR Fitness Band',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '148',
                'category_id' => '4',
                'sub_category_id' => '37',
                'name' => 'Fastrack reflex 2.0',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '149',
                'category_id' => '5',
                'sub_category_id' => '38',
                'name' => 'Kisna Real Diamond Jewellery Gold Diamond',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '150',
                'category_id' => '5',
                'sub_category_id' => '38',
                'name' => 'Karatcart Platinum Plated',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '151',
                'category_id' => '5',
                'sub_category_id' => '38',
                'name' => 'Jewels Galaxy',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '152',
                'category_id' => '5',
                'sub_category_id' => '38',
                'name' => 'Yellow Chimes Dragon Celtic',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '153',
                'category_id' => '5',
                'sub_category_id' => '39',
                'name' => 'Stone Metal Scissors Sea Shells',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '154',
                'category_id' => '5',
                'sub_category_id' => '39',
                'name' => 'Estele 24 Kt Rose Gold Plated',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '155',
                'category_id' => '5',
                'sub_category_id' => '39',
                'name' => 'Hot And Bold Long Distance',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '156',
                'category_id' => '5',
                'sub_category_id' => '39',
                'name' => 'YouBella Jewellery Bracelets for Women',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '157',
                'category_id' => '5',
                'sub_category_id' => '40',
                'name' => 'P.C. Chandra Jewellers',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '158',
                'category_id' => '5',
                'sub_category_id' => '40',
                'name' => 'Diamond Stud Earrings for Women',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '159',
                'category_id' => '5',
                'sub_category_id' => '40',
                'name' => 'Yellow Gold Earrings for Women',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '160',
                'category_id' => '5',
                'sub_category_id' => '40',
                'name' => 'Gold Plated Wedding Jewellery',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '161',
                'category_id' => '5',
                'sub_category_id' => '41',
                'name' => 'Silver Round American Diamond Necklace',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '162',
                'category_id' => '5',
                'sub_category_id' => '41',
                'name' => 'Sets for Women & Girls',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '163',
                'category_id' => '5',
                'sub_category_id' => '41',
                'name' => 'Silver Design American Diamond Necklace',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '164',
                'category_id' => '5',
                'sub_category_id' => '41',
                'name' => 'Matushri Art Brass and Ruby Jewellery Set for Women',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '165',
                'category_id' => '6',
                'sub_category_id' => '42',
                'name' => 'GREY JACK Polarized Classic Aviator',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '166',
                'category_id' => '6',
                'sub_category_id' => '42',
                'name' => 'grey jack Polarized Sunglasses for Men',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '167',
                'category_id' => '6',
                'sub_category_id' => '42',
                'name' => 'Fastrack Men Square Sunglasses Black Frame',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '168',
                'category_id' => '6',
                'sub_category_id' => '42',
                'name' => 'Fastrack UV Protected Square Mens',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '169',
                'category_id' => '6',
                'sub_category_id' => '43',
                'name' => 'U.S Desire Blue Light Blocking Glasses',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '170',
                'category_id' => '6',
                'sub_category_id' => '43',
                'name' => 'Faddish Unisex Rectangular Computer Glasses',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '171',
                'category_id' => '6',
                'sub_category_id' => '43',
                'name' => 'Blue Light Blocking Glasses',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '172',
                'category_id' => '6',
                'sub_category_id' => '43',
                'name' => 'Dervin Mens & Boys Square Sunglasses',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '173',
                'category_id' => '6',
                'sub_category_id' => '44',
                'name' => 'Eyewearlabs Mens Blue Sunglasses',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '174',
                'category_id' => '6',
                'sub_category_id' => '44',
                'name' => 'Eyewearlabs Polarized Square Men Sunglasses',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '175',
                'category_id' => '6',
                'sub_category_id' => '44',
                'name' => 'GREY JACK Square Polarized Sunglasses for Men',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '176',
                'category_id' => '6',
                'sub_category_id' => '44',
                'name' => 'Royal Son Retro Square Sunglasses For Men',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '177',
                'category_id' => '7',
                'sub_category_id' => '45',
                'name' => 'Harry Tonnish Black Leather Mens Wallet',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '178',
                'category_id' => '7',
                'sub_category_id' => '45',
                'name' => 'Hornbull Denial Olive Green Mens Leather',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '179',
                'category_id' => '7',
                'sub_category_id' => '45',
                'name' => 'Rigohill Harry Rustic Brown Mens Leather',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '180',
                'category_id' => '7',
                'sub_category_id' => '45',
                'name' => 'WildHorn® Leather Wallet for Men',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '181',
                'category_id' => '7',
                'sub_category_id' => '46',
                'name' => 'LORENZ Grey Fabric Mens Wallet (1955)',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '182',
                'category_id' => '7',
                'sub_category_id' => '46',
                'name' => 'Atrangi.org Beige Fabric Unisex Wallet',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '183',
                'category_id' => '7',
                'sub_category_id' => '46',
                'name' => 'Poland Jeans Fabric Mens Wallet',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '184',
                'category_id' => '7',
                'sub_category_id' => '46',
                'name' => 'KELILE INDIA Fabric Wallet for Men',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '185',
                'category_id' => '7',
                'sub_category_id' => '47',
                'name' => 'Storite 6 Slots RFID Blocking Metal Credit',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '186',
                'category_id' => '7',
                'sub_category_id' => '47',
                'name' => 'Sangram fabric wallet for men',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '187',
                'category_id' => '7',
                'sub_category_id' => '47',
                'name' => 'Mens Grey Money Clip and Credit Card',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '188',
                'category_id' => '7',
                'sub_category_id' => '47',
                'name' => 'Metal Wallet Money Clip',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '189',
                'category_id' => '7',
                'sub_category_id' => '48',
                'name' => 'BOGESI Blue Polyester Mens Wallet',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '190',
                'category_id' => '7',
                'sub_category_id' => '48',
                'name' => 'Puma Tan Polyester Unisex Wallet',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '191',
                'category_id' => '7',
                'sub_category_id' => '48',
                'name' => 'Puma Red Polyester Unisex Wallet',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '192',
                'category_id' => '7',
                'sub_category_id' => '48',
                'name' => 'Hornbull Taylor Navy/Mud Mens Leather Wallet',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '193',
                'category_id' => '8',
                'sub_category_id' => '49',
                'name' => 'Fargo Handbag For Women And Girls COMBO',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '194',
                'category_id' => '8',
                'sub_category_id' => '49',
                'name' => 'Care4u Handbag For Women And Girls COMBO',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '195',
                'category_id' => '8',
                'sub_category_id' => '49',
                'name' => 'Fiesto Fashion Womens Handbag',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],
            [
                'id' => '196',
                'category_id' => '8',
                'sub_category_id' => '49',
                'name' => 'Fargo PU Leather Latest Handbags For Womens',
                'price' => '529',
                'description' => 'Care Instructions: Wash with mild detergent, do not bleach, dry in shade
                Fit Type: regular fit
                60% cotton and 40% polyester
                Regular fit
                Banded collar
                Half sleeve
                Wash with mild detergent, do not bleach, dry in shade
                Made in India
                Plus size available from size XXL onwards
                Country of Origin: India',
                'quantity' => '2',
                'status' => 'active'
            ],

        ]);
        collect($Products)->each(function ($Products) {
            Product::create($Products);
        });
    }
}
