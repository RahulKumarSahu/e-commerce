<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class SocialLinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
 DB::table('social_links')->insert(array(
       array(
                'facebook_url'=>'https://www.facebook.com/',
                'twitter_url'=>'https://twitter.com/login?lang=en',
                'pinterest_url' => 'https://in.pinterest.com/login/',
                'youtube_url'=>'https://www.youtube.com/',
                'linkedin_url'=>'https://www.linkedin.com/',
                


       )));

    }
}
