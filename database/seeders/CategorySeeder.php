<?php
namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ([
            [
                'id' => '1',
                'name' => 'Clothing',
                'featured' => '0',
                'status' => 'active'
            ],
            [
                 'id' => '2',
                'name' => 'Footwear',
                'featured' => '1',
                'status' => 'active'
            ],
            [
                 'id' => '3',
                'name' => 'Beauty',
                'featured' => '1',
                'status' => 'active'
            ],
            [
                 'id' => '4',
                'name' => 'Watches',
                'featured' => '1',
                'status' => 'active'
            ],
            [
                 'id' => '5',
                'name' => 'Jewellery',
                'featured' => '0',
                'status' => 'active'
            ],
            [
                 'id' => '6',
                'name' => 'Eyewear',
                'featured' => '1',
                'status' => 'active'
            ],
            [
                 'id' => '7',
                'name' => 'Wallets',
                'featured' => '1',
                'status' => 'active'
            ],
            [
                 'id' => '8',
                'name' => 'Hangbags & Clutches',
                'featured' => '0',
                'status' => 'active'
            ],
        ]);
        collect($categories)->each(function ($category) {
            Category::create($category);
        });
    }
}
