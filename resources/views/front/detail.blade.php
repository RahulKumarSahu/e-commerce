@extends('layouts.front')

@section('title')
    Detail
@endsection

@section('body')

    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Clothing</a></li>
                    <li class='active'>Floral Print Buttoned</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->

    <div class="body-content outer-top-xs">
        <div class='container'>
            <div class='row single-product'>
                <div class='col-xs-12 col-sm-12 col-md-12'>
                    <div class="detail-block">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 gallery-holder">
                                <div class="product-item-holder size-big single-product-gallery small-gallery">
                                    <div id="owl-single-product">
                                        <div class="single-product-gallery-item" id="slide1">
                                            <a data-lightbox="image-1" data-title="Gallery"
                                                href="{{ $products->product_photo }}">
                                                <img class="img-responsive" alt=""
                                                    src="{{ $products->product_photo }}"
                                                    data-echo="{{ $products->product_photo }}" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->
                                        <div class="single-product-gallery-item" id="slide2">
                                            <a data-lightbox="image-1" data-title="Gallery"
                                                href="{{ $products->product_photo }}">
                                                <img class="img-responsive" alt=""
                                                    src="{{ $products->product_photo }}"
                                                    data-echo="{{ $products->product_photo }}" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->
                                        <div class="single-product-gallery-item" id="slide3">
                                            <a data-lightbox="image-1" data-title="Gallery"
                                                href="{{ $products->product_photo }}">
                                                <img class="img-responsive" alt=""
                                                    src="{{ $products->product_photo }}"
                                                    data-echo="{{ $products->product_photo }}" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->
                                        <div class="single-product-gallery-item" id="slide4">
                                            <a data-lightbox="image-1" data-title="Gallery"
                                                href="{{ asset('front-assets/images/products/p4.jpg') }}">
                                                <img class="img-responsive" alt=""
                                                    src="{{ asset('front-assets/images/blank.gif') }}"
                                                    data-echo="{{ asset('front-assets/images/products/p4.jpg') }}" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->
                                        <div class="single-product-gallery-item" id="slide5">
                                            <a data-lightbox="image-1" data-title="Gallery"
                                                href="{{ asset('front-assets/images/products/p5.jpg') }}">
                                                <img class="img-responsive" alt=""src="{{ asset('front-assets/images/blank.gif') }}"data-echo="{{ asset('front-assets/images/products/p5.jpg') }}" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->
                                        <div class="single-product-gallery-item" id="slide6">
                                            <a data-lightbox="image-1" data-title="Gallery"
                                                href="{{ asset('front-assets/images/products/p6.jpg') }}">
                                                <img class="img-responsive" alt=""src="{{ asset('front-assets/images/blank.gif') }}"data-echo="{{ asset('front-assets/images/products/p6.jpg') }}" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->

                                        <div class="single-product-gallery-item" id="slide7">
                                            <a data-lightbox="image-1" data-title="Gallery"
                                                href="{{ asset('front-assets/images/products/p7.jpg') }}">
                                                <img class="img-responsive" alt=""src="{{ asset('front-assets/images/blank.gif') }}" data-echo="{{ asset('front-assets/images/products/p7.jpg') }}" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->

                                        <div class="single-product-gallery-item" id="slide8">
                                            <a data-lightbox="image-1" data-title="Gallery"
                                                href="{{ asset('front-assets/images/products/p8.jpg') }}">
                                                <img class="img-responsive" alt=""src="{{ asset('front-assets/images/blank.gif') }}"data-echo="{{ asset('front-assets/images/products/p8.jpg') }}" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->

                                        <div class="single-product-gallery-item" id="slide9">
                                            <a data-lightbox="image-1" data-title="Gallery"
                                                href="{{ asset('front-assets/images/products/p9.jpg') }}">
                                                <img class="img-responsive" alt=""src="{{ asset('front-assets/images/blank.gif') }}"data-echo="{{ asset('front-assets/images/products/p9.jpg') }}" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->

                                    </div><!-- /.single-product-slider -->

                                    <div class="single-product-gallery-thumbs gallery-thumbs">
                                        <div id="owl-single-product-thumbnails">
                                            <div class="item">
                                                <a class="horizontal-thumb active" data-target="#owl-single-product"
                                                    data-slide="1" href="#slide1">
                                                    <img class="img-responsive" alt=""
                                                    src="{{ $products->product_photo }}" data-echo="{{ $products->product_photo }}" />
                                                </a>
                                            </div>
                                            <div class="item">
                                                <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="2"
                                                    href="#slide2">
                                                    <img class="img-responsive" alt=""
                                                    src="{{ asset('front-assets/images/blank.gif') }}"data-echo="{{ asset('front-assets/images/products/p2.jpg') }}" />
                                                </a>
                                            </div>
                                            <div class="item">
                                                <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="3"
                                                    href="#slide3">
                                                    <img class="img-responsive" alt=""
                                                    src="{{ asset('front-assets/images/blank.gif') }}" data-echo="{{ asset('front-assets/images/products/p3.jpg') }}" />
                                                </a>
                                            </div>
                                            <div class="item">
                                                <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="4"
                                                    href="#slide4">
                                                    <img class="img-responsive" alt=""
                                                    src="{{ asset('front-assets/images/blank.gif') }}" data-echo="{{ asset('front-assets/images/products/p4.jpg') }}" />
                                                </a>
                                            </div>
                                            <div class="item">

                                                <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="5"
                                                    href="#slide5">
                                                    <img class="img-responsive" alt=""
                                                    src="{{ asset('front-assets/images/blank.gif') }}"
                                                    data-echo="{{ asset('front-assets/images/products/p5.jpg') }}" />
                                                </a>
                                            </div>
                                            <div class="item">

                                                <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="6"
                                                    href="#slide6">
                                                    <img class="img-responsive" alt=""
                                                    src="{{ asset('front-assets/images/blank.gif') }}"data-echo="{{ asset('front-assets/images/products/p6.jpg') }}" />
                                                </a>
                                            </div>
                                            <div class="item">

                                                <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="7"
                                                    href="#slide7">
                                                    <img class="img-responsive" alt=""src="{{ asset('front-assets/images/blank.gif') }}" data-echo="{{ asset('front-assets/images/products/p7.jpg') }}" />
                                                </a>
                                            </div>
                                            <div class="item">
                                                <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="8"
                                                    href="#slide8">
                                                    <img class="img-responsive" alt="" src="{{ $products->product_photo }}"data-echo="{{ $products->product_photo }}" />
                                                </a>
                                            </div>
                                            <div class="item">
                                                <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="9"
                                                    href="#slide9">
                                                    <img class="img-responsive" alt=""src="{{ asset('front-assets/images/blank.gif') }}"data-echo="{{ asset('front-assets/images/products/p9.jpg') }}" />
                                                </a>
                                            </div>
                                        </div><!-- /#owl-single-product-thumbnails -->
                                    </div><!-- /.gallery-thumbs -->

                                </div><!-- /.single-product-gallery -->
                            </div><!-- /.gallery-holder -->

                            <div class='col-sm-12 col-md-8 col-lg-8 product-info-block'>
                                <div class="product-info">
                                    <h1 class="name">{{ $products->name }}</h1>
                                    <div class="stock-container info-container m-t-10">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="pull-left">
                                                    <div class="stock-box">
                                                        <span class="label">Availability : </span>
                                                    </div>
                                                </div>
                                                <div class="pull-left">
                                                    <div class="stock-box">
                                                        <span class="value">{{ $products->status }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- /.row -->
                                    </div><!-- /.stock-container -->

                                    <div class="description-container m-t-20">
                                        <p>{{ $products->description }}</p>
                                    </div><!-- /.description-container -->
                                    <div class="price-container info-container m-t-30">
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6">
                                                <div class="price-box">
                                                    <span class="price">${{ $products->price }}</span>
                                                    <span class="price-strike">$900.00</span>
                                                </div>
                                            </div>
                                        </div><!-- /.row -->
                                    </div><!-- /.price-container -->
                                    <div class="quantity-container info-container">
                                        <div class="row">
                                            <div class="qty">
                                                <span class="label">Qty :</span>
                                            </div>
                                            <div class="qty-count">
                                                <div class="cart-quantity">
                                                    <div class="quant-input">
                                                        <div class="arrows">
                                                            <div class="arrow plus gradient"><span class="ir"><i
                                                            class="icon fa fa-sort-asc"></i></span></div>
                                                            <div class="arrow minus gradient"><span class="ir"><i
                                                            class="icon fa fa-sort-desc"></i></span></div>
                                                        </div>
                                                        <input type="text" value="0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="add-btn">
                                                <a href="#" class="btn btn-primary"><i
                                                class="fa fa-shopping-cart inner-right-vs"></i> ADD TO CART</a>
                                            </div>
                                        </div><!-- /.row -->
                                    </div><!-- /.quantity-container -->
                                </div><!-- /.product-info -->
                            </div><!-- /.col-sm-7 -->
                        </div><!-- /.row -->
                    </div>
                    <div class="product-tabs inner-bottom-xs">
                        <div class="row">
                            <div class="col-sm-12 col-md-9 col-lg-9">
                                <div class="tab-content">
                                    <div id="tags" class="tab-pane">
                                        <div class="product-tag">
                                            <h4 class="title">Product Tags</h4>
                                            <form class="form-inline form-cnt">
                                                <div class="form-container">
                                                    <div class="form-group">
                                                        <label for="exampleInputTag">Add Your Tags: </label>
                                                        <input type="email" id="exampleInputTag" class="form-control txt">
                                                    </div>
                                                    <button class="btn btn-upper btn-primary" type="submit">ADD TAGS</button>
                                                </div><!-- /.form-container -->
                                            </form><!-- /.form-cnt -->
                                            <form class="form-inline form-cnt">
                                                <div class="form-group">
                                                    <label>&nbsp;</label>
                                                    <span class="text col-md-offset-3">Use spaces to separate tags. Use
                                                        single quotes (') for phrases.</span>
                                                </div>
                                            </form><!-- /.form-cnt -->
                                        </div><!-- /.product-tab -->
                                    </div><!-- /.tab-pane -->
                                </div><!-- /.tab-content -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.product-tabs -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.body-content -->
@endsection
