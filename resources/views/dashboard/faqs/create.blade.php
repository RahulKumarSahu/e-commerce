@extends('layouts.dashboard')
@section('title')
    Create Faq
@endsection
@section('content')

    <body onload="createFaq()">
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create Faq</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('faqs.store') }}" method="post" class="panel-body">
                            @csrf
                            <div class="wrapper wrapper-content animated fadeInRight">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group @error('title') has-error @enderror">
                                            <label for="title" class="control-label">Title <span
                                                    class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter title here..." class="form-control"
                                                name="title" id="title">
                                            @error('title')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group @error('description') has-error @enderror">
                                            <label for="description" class="control-label">Description <span
                                                    class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter description here..."
                                                class="form-control" name="description" id="description">
                                            @error('description')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group @error('status') has-error @enderror">
                                            <label for="status">Status <span class="text-danger">*</span></label>
                                            <select class="form-control" name="status" id="state">
                                                <option selected disabled>Select</option>
                                                <option value="active">active</option>
                                                <option value="inactive">inactive</option>
                                            </select>
                                            @error('status')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary"><span
                                        class="fa fa-save"></span>Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function createFaq() {
                $('#exampleModal').modal('show');
            }
        </script>
    </body>

@endsection
