@extends('layouts.dashboard')
@section('title')
    Show Faq
@endsection
@section('content')

    <body onload="showFaq()">
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalLabel">Show FAQ</h5>
                        </div>
                        <div class="col-lg-2">
                            <a href="/settings" class="btn btn-warning">
                                <span class="fa fa-arrow-left"></span>
                                Back
                            </a>
                        </div>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('faqs.show', $faqs->id) }}" method="post" class="panel-body">
                            @csrf
                            <div class="wrapper wrapper-content animated fadeInRight">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group @error('title') has-error @enderror">
                                            <label for="title" class="control-label">Title <span
                                                    class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter title here..." class="form-control"
                                                name="title" id="title" value="{{ $faqs->title }}">
                                            @error('title')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group @error('description') has-error @enderror">
                                            <label for="description" class="control-label">Description <span
                                                    class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter description here..."
                                                class="form-control" name="description" id="description"
                                                value="{{ $faqs->description }}">
                                            @error('description')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group @error('status') has-error @enderror">
                                            <label for="status">Status <span class="text-danger">*</span></label>
                                            <select class="form-control" name="status" id="status">
                                                <option>Select</option>
                                                <option value="active" {{ $faqs->status == 'active' ? 'selected' : '' }}>
                                                    Active</option>
                                                <option value="inactive"
                                                    {{ $faqs->status == 'inactive' ? 'selected' : '' }}>Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function showFaq() {
                $('#exampleModal').modal('show');
            }
        </script>
    </body>
@endsection
