@extends('layouts.dashboard')

@section('title')
    Show Category
@endsection
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"
        integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="{{ asset('asset/css/plugins/switchery/switchery.css') }}" rel="stylesheet">

@endsection

@section('breadcrum')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-11">
            <h2>Show Category </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Home</a>
                </li>

                <li>
                    <a href="{{ route('sub-categories.index') }}">Sub Category</a>
                </li>

                <li class="active">
                    <strong>Show Category </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-1">
            <a href="{{ route('sub-categories.index') }}" class="btn btn-warning mt-3" id="save-data">
                <span class="fa fa-arrow-left"></span>
                Back
            </a>
        </div>
    </div>
@endsection

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <form action="{{ route('sub-categories.show', $subCategories->id) }}" method="post"
                enctype="multipart/form-data">
                @csrf
                <div class="col-lg-9">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Sub Categories Details</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group @error('status') has-error @enderror">
                                        <label for="status">Category <span class="text-danger">*</span></label>
                                        <select class="form-control" name="category_id" id="category_id" disabled>
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}"@if($category->id == $subCategories->id) selected @endif>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('name') has-error @enderror">
                                        <label for="name"> Sub Category <span
                                                class="text-danger">*</span></label>
                                        <input type="text" class="form-control"
                                            placeholder="Enter name" name="name"
                                            id="name" value="{{ $subCategories->name }}" disabled>
                                        @error('name')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('status') has-error @enderror">
                                        <label for="status">Status <span class="text-danger">*</span></label>
                                        <select class="form-control" name="status" id="state" disabled>
                                            <option>Select</option>
                                            <option value="active"
                                                {{ $subCategories->status == 'active' ? 'selected' : '' }}>active</option>
                                            <option value="inactive"
                                                {{ $subCategories->status == 'inactive' ? 'selected' : '' }}>inactive
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="slug">Slug <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="slug" id="slug"
                                        value="{{ $subCategories->slug }}" readonly>
                            </div>
                            <div class="col-md-12 mt-2">
                                <div class="form-group">
                                    <label for="featured">Featured</label>
                                    <div class="enable">
                                        <input type="hidden" value="0"
                                            @if ($subCategories->featured == '0')
                                            checked
                                            @endif
                                        id="checkBox1" name="featured" class="js-switch_3" />
                                        <input type="checkbox" value="1"
                                            @if ($subCategories->featured == '1')
                                            checked
                                            @endif
                                        id="checkBox" name="featured" class="js-switch_3" />
                                    </div>
                                 </div>
                            </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Photo</h5>
                        </div>
                        <div class="ibox-content">
                            <input type="file" class="dropify" name="file"
                                data-default-file="{{ asset('sub-categories-uploads/sub-categories/' . $subCategories->photo) }}" disabled/>
                        </div>
                    </div>
                    <div>
                    </div>
                </div>

            </form>
        </div>
    </div>



@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
        integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- Switchery -->
    <script src="{{ asset('asset/js/plugins/switchery/switchery.js') }}"></script>

    <script>
        $('.dropify').dropify();

        var elem_3 = document.querySelector('.js-switch_3');
            var switchery_3 = new Switchery(elem_3, {
                color: '#1AB394'
            });

            $('#checkBox').on('change', function(){
             this.value = this.checked ? 1 : 0;
                // alert(this.value);
            }).change();

        $(document).on('click', '#save-data', function() {
            $('form').submit();
        })
    </script>

@endsection
