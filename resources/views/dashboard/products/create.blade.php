@extends('layouts.dashboard')

@section('title')
    Add Product
@endsection

@section('css')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/dropzone.min.css" integrity="sha512-jU/7UFiaW5UBGODEopEqnbIAHOI8fO6T99m7Tsmqs2gkdujByJfkCbbfPSN4Wlqlb9TGnsuC0YgUgWkRBK7B9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="{{ asset('asset/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('asset/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">

    <link href="{{ asset('asset/css/plugins/dropzone/basic.css') }}" rel="stylesheet">
    <link href="{{ asset('asset/css/plugins/dropzone/dropzone.css') }}" rel="stylesheet">
@endsection
@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Add Products</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/home">Home</a>
                </li>

                <li>
                    <a href="{{ route('products.index') }}">Products</a>
                </li>
                <li class="active">
                    <strong>Add Product</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <a href="{{ route('products.index') }}" class="btn btn-warning mt-3" id="save-data">
                <span class="fa fa-arrow-left"></span>
                Back
            </a>
            <button class="btn btn-primary mt-3" id="save-data"> <span class="fa fa-save"></span> Save</button>
        </div>

    </div>
@endsection

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <form action="{{ route('products.store') }}"  id="image-upload" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Product Details</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group @error('category_id') has-error @enderror">
                                        <label for="category_id">Category <span class="text-danger">*</span></label>
                                        <select class="form-control" name="category_id" id="category_id">
                                            <option>Select</option>
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="category_id">Sub-Category </label>
                                        <select class="form-control" name="sub_category_id" id="sub_category_id">
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('product_name') has-error @enderror">
                                        <label for="product_name">Product Name <span class="text-danger">*</span></label>
                                        <input type="text" placeholder="Enter product_name" class="form-control" name="name"
                                            id="product_name" value="{{ old('product_name') }}">
                                        @error('product_name')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('price') has-error @enderror">
                                        <label for="price">Price <span class="text-danger">*</span></label>
                                        <input type="price" class="form-control" placeholder="Enter price" name="price"
                                            id="price" value="{{ old('price') }}">
                                        @error('price')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('discount') has-error @enderror">
                                        <label for="discount">Discount </label>
                                        <input type="number" class="form-control" placeholder="Enter discount in %" name="discount"
                                            id="discount" value="{{ old('discount') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('quantity') has-error @enderror">
                                        <label for="quantity">Quantity <span class="text-danger">*</span></label>
                                        <input type="number" class="form-control" placeholder="Enter quantity"
                                            name="quantity" id="quantity" value="{{ old('quantity') }}">
                                        @error('quantity')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12 mt-2">
                                    <div class="form-group @error('featured') has-error @enderror">
                                        <label for="featured">Featured</label>
                                        <div class="enable ">
                                            <input type="hidden" value="0" id="checkBox1" name="featured" class="js-switch_3" />
                                            <input type="checkbox" value="1" id="checkBox" name="featured" class="js-switch_3" />
                                        </div>
                                     </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('description') has-error @enderror">
                                        <label for="description">Description <span class="text-danger">*</span></label>
                                        <textarea class="form-control summernote" name="description" id="description">
                                        </textarea>
                                        @error('description')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('status') has-error @enderror">
                                        <label for="status">Status <span class="text-danger">*</span></label>
                                        <select class="form-control" name="status" id="state">
                                            <option>Select</option>
                                            <option value="active">active</option>
                                            <option value="inactive">inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Multi Photo</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <input type="file" name="file" multiple="" data-default-file=/>
                                        </div>
                                    </div>
                                    <div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>

@endsection
@section('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/min/dropzone.min.js" integrity="sha512-VQQXLthlZQO00P+uEu4mJ4G4OAgqTtKG1hri56kQY1DtdLeIqhKUp9W/lllDDu3uN3SnUNawpW7lBda8+dSi7w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{ asset('asset/js/plugins/switchery/switchery.js') }}"></script>

    <script src="{{ asset('js/dashboard.js') }}"></script>
    <script src="{{ asset('asset/js/jquery-2.1.1.js') }}"></script>
    <script src="{{ asset('asset/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('asset/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('asset/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
         <!-- Custom and plugin javascript -->
    <script src="{{ asset('js/inspinia.js') }}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>
    <!-- SUMMERNOTE -->
    <script src="{{ asset('asset/js/plugins/summernote/summernote.min.js') }}"></script>
    <script>

        $(document).ready(function() {
        $('.summernote').summernote();

        });

        var edit = function() {
            $('.click2edit').summernote({
                focus: true
            });
        };
        var save = function() {
            var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
            $('.click2edit').destroy();
        };

        $(document).on('click', '#save-data', function() {
            $('form').submit();
        });

        $(document).on('change', '#category_id', function() {

            var categoryId = $(this).val();

            let url = "{{ route('get_sub_categories', ':id') }}";
            url = url.replace(':id', categoryId);

            $.ajax({
                url: url,
                type: 'GET',
                success: function(response) {
                    $('#sub_category_id').html(response);
                }
            });

        });

    </script>

@endsection
