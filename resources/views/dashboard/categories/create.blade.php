@extends('layouts.dashboard')

@section('title')
    Add category
@endsection
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"
        integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

@endsection
@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Add Category</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/home">Home</a>
                </li>

                <li>
                    <a href="{{ route('categories.index') }}">Category</a>
                </li>

                <li class="active">
                    <strong>Add Category</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <a href="{{ route('categories.index') }}" class="btn btn-warning mt-3" id="save-data">
                <span class="fa fa-arrow-left"></span>
                Back
            </a>
            <button class="btn btn-primary mt-3" id="save-data"> <span class="fa fa-save"></span> Save</button>
        </div>
    </div>
@endsection

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <form action="{{ route('categories.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col-lg-9">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Category Details</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group @error('name') has-error @enderror">
                                        <label for="product_category">name <span
                                                class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter Name" name="name"
                                            id="product_category" value="{{ old('name') }}">
                                            @error('name')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('status') has-error @enderror">
                                        <label for="status">Status <span class="text-danger">*</span></label>
                                        <select class="form-control" name="status" id="state">
                                            <option selected disabled>Select</option>
                                            <option value="active">active</option>
                                            <option value="inactive">inactive</option>
                                        </select>
                                        @error('status')
                                        <span class="text-danger"> {{ $message }} </span>
                                    @enderror
                                    </div>
                                </div>
                                <div class="col-md-12 mt-2">
                                    <div class="form-group">
                                        <label for="featured">Featured</label>
                                        <div class="enable ">
                                            <input type="hidden" value="0" id="checkBox1" name="featured" class="js-switch_3" />
                                            <input type="checkbox" value="1" id="checkBox" name="featured" class="js-switch_3" />
                                        </div>
                                     </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                 <div class="col-lg-3">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Photo</h5>
                        </div>
                        <div class="ibox-content">
                            <input type="file" class="dropify" name="file" data-default-file=/>
                        </div>
                    </div>
                    <div>
                    </div>
                </div>

            </form>
        </div>
    </div>

@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
        integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="{{ asset('asset/js/plugins/switchery/switchery.js') }}"></script>
        <script src="{{ asset('asset/js/plugins/dropzone/dropzone.js') }}"></script>

    <script>
        $('.dropify').dropify();

        $(document).on('click', '#save-data', function() {
            $('form').submit();
        })

        var elem_3 = document.querySelector('.js-switch_3');
        var switchery_3 = new Switchery(elem_3, {
            color: '#1AB394'
        });
     </script>
@endsection

