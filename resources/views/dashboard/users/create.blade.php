@extends('layouts.dashboard')

@section('title')
    Add User
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"
        integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Add User </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="{{ route('users.index') }}">Users</a>
                </li>
                <li class="active">
                    <strong>Add User</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <a href="{{ route('users.index') }}" class="btn btn-warning mt-3" id="save-data">
                <span class="fa fa-arrow-left"></span>
                Back
            </a>
            <button class="btn btn-primary mt-3" id="save-data"> <span class="fa fa-save"></span> Save</button>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <form action="{{ route('users.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-lg-9">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Users Details</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group @error('name') has-error @enderror">
                                        <label for="name">Name <span class="text-danger">*</span></label>
                                        <input type="text" placeholder="Enter name" class="form-control" name="name"
                                            id="name" value="{{ old('name') }}">
                                        @error('name')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('email') has-error @enderror">
                                        <label for="email">Email <span class="text-danger">*</span></label>
                                        <input type="email" class="form-control" placeholder="Enter email" name="email"
                                            id="email" value="{{ old('email') }}">
                                        @error('email')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('password') has-error @enderror">
                                        <label for="password">Password <span class="text-danger">*</span></label>
                                        <input type="password" class="form-control" placeholder="Enter password"
                                            name="password" id="password" value="{{ old('password') }}">
                                        @error('password')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('phone') has-error @enderror">
                                        <label for="phone">Phone No. <span class="text-danger">*</span></label>
                                        <input type="number" class="form-control" placeholder="Enter phone no"
                                            name="phone" id="phone" value="{{ old('phone') }}">
                                        @error('password')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Photo</h5>
                        </div>
                        <div class="ibox-content">
                            <input type="file" class="dropify" name="file" />
                        </div>
                    </div>
                    <div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
        integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew=="
        crossorigin="anonymous" referrerpolicy="no-referrer">
    </script>

    <script>
        $('.dropify').dropify();


        $(document).on('click', '#save-data', function() {
            $('form').submit();
        });
    </script>

@endsection
