@extends('layouts.dashboard')

@section('title')
    Add Address
@endsection
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Add Address Form</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/setting-index">Home</a>
                </li>
                <li class="active">
                    <strong>Add Address</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <button class="btn btn-primary mt-3" id="save-data" style="margin-top: 3rem"> <span class="fa fa-save"></span>
                Save Address</button>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <form action="" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Address Details</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group @error('country') has-error @enderror">
                                        <label for="country">Country <span class="text-danger">*</span></label>
                                        <input type="text" placeholder="Enter country name" class="form-control"
                                            name="country" id="country" value="{{ old('country') }}">
                                        @error('country')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('state') has-error @enderror">
                                        <label for="state">State <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter state" name="state"
                                            id="state" autocomplete="off" value="{{ old('state') }}">
                                        @error('state')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('city') has-error @enderror">
                                        <label for="city">City <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter city" name="city"
                                            id="city" value="{{ old('city') }}">
                                        @error('city')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('pin') has-error @enderror">
                                        <label for="pin">Pin Code<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter pin" name="pin" id="pin"
                                            value="{{ old('pin') }}">
                                        @error('pin')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('area') has-error @enderror">
                                        <label for="area">Area <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter area" name="area"
                                            id="area" value="{{ old('area') }}">
                                        @error('area')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group @error('landmark') has-error @enderror">
                                        <label for="landmark">Landmark <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter landmark" name="landmark"
                                            id="landmark" value="{{ old('landmark') }}">
                                        @error('landmark')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
