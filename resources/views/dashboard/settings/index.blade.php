@extends('layouts.dashboard')

@section('title')
    Settings
@endsection
@section('css')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"
        integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="{{ asset('asset/css/plugins/switchery/switchery.css') }}" rel="stylesheet">
    <link href="{{ asset('asset/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('asset/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="wrapper wrapper-content animated fadeIn">
        <div class="tabs-container">
            <div class="tabs-left">
                <ul class="nav nav-tabs">
                    {{-- Tab Menu's --}}
                    <li class="active"><a data-toggle="tab" href="#tab-1">General Settings</a></li>
                    <li class="___class_+?5___"><a data-toggle="tab" href="#tab-2">Language Settings</a></li>
                    <li class="___class_+?6___"><a data-toggle="tab" href="#tab-3">SMTP Settings</a></li>
                    <li class="___class_+?7___"><a data-toggle="tab" href="#tab-4">SMS Settings</a></li>
                    <li class="___class_+?8___"><a data-toggle="tab" href="#tab-5">Addresses</a></li>
                    <li class="___class_+?9___"><a data-toggle="tab" href="#tab-6">Social Links</a></li>
                    <li class="___class_+?10___"><a data-toggle="tab" href="#tab-7">Payment Settings</a></li>
                    <li class="___class_+?11___"><a data-toggle="tab" href="#tab-8">FAQs</a></li>
                </ul>
                <div class="tab-content ">
                    {{-- General Settings --}}
                    <div id="tab-1" class="tab-pane active">
                        <form action="{{ route('general-settings.update', $generalSettings->id) }}" method="POST"
                            enctype="multipart/form-data" class="panel-body">
                            @csrf
                            @method('PUT')
                            <div class="row wrapper border-bottom white-bg page-heading" style="height: 9rem">
                                <div class="col-lg-10">
                                    <h2>General Settings</h2>
                                    <ol class="breadcrumb">
                                        <li>
                                            <a href="/home">Home</a>
                                        </li>
                                        <li>
                                            <a href="/settings">Settings</a>
                                        </li>
                                        <li class="active">
                                            <strong>General Settings</strong>
                                        </li>
                                    </ol>
                                </div>
                                <div class="col-lg-1 mt-3">
                                    <button type="submit" class="btn btn-primary">
                                        <span class="fa fa-save"></span>
                                        Save</button>
                                </div>
                            </div>
                            <div class="wrapper wrapper-content animated fadeInRight">
                                <div class="row" style="margin:0px">
                                    <div class="col-sm-6">
                                        <div class="form-group @error('site_name') has-error @enderror">
                                            <label for="name" class="control-label"> Site Name <span
                                                    class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter site name" class="form-control"
                                                name="site_name" id="site_name" value="{{ $generalSettings->site_name }}">
                                            @error('site_name')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group @error('language') has-error @enderror">
                                            <label for="language">Language <span class="text-danger">*</span></label>
                                            <select class="form-control" name="language" id="language">
                                                @foreach ($languages as $language)
                                                    <option value="{{ $language->code }}">{{ $language->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="site_logo" class="control-label"> Site Logo</label>
                                            <input type="file" class="dropify" name="site_logo"
                                                data-default-file="{{ asset('GeneralSetting-uploads/GeneralSetting/' . $generalSettings->site_logo) }}" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="favicon_logo" class="control-label">Favicon Logo</label>
                                            <input type="file" class="dropify" name="favicon_logo"
                                                data-default-file="{{ asset('GeneralSetting-uploads/GeneralSetting/' . $generalSettings->favicon_logo) }}" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group @error('title') has-error @enderror">
                                            <label for="title" class="control-label"> Title<span
                                                    class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter title" class="form-control" name="title"
                                                id="title" value="{{ $generalSettings->title }}">
                                            @error('title')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group @error('address') has-error @enderror">
                                            <label for="address" class="control-label"> Address<span
                                                    class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter address" class="form-control"
                                                name="address" id="address" value="{{ $generalSettings->address }}">
                                            @error('address')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group @error('contact1') has-error @enderror">
                                            <label for="contact1" class="control-label"> Contact 1<span
                                                    class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter contact1" class="form-control"
                                                name="contact1" id="contact1" value="{{ $generalSettings->contact1 }}">
                                            @error('contact1')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group @error('contact2') has-error @enderror">
                                            <label for="contact2" class="control-label"> Contact 2<span
                                                    class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter contact2" class="form-control"
                                                name="contact2" id="contact2" value="{{ $generalSettings->contact2 }}">
                                            @error('contact2')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group @error('email') has-error @enderror">
                                            <label for="email" class="control-label">Email<span
                                                    class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter email" class="form-control" name="email"
                                                id="email" value="{{ $generalSettings->email }}">
                                            @error('email')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group @error('terms_and_conditions') has-error @enderror">
                                            <label for="terms_and_conditions" class="control-label">Terms and
                                                Conditions<span class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter terms_and_conditions"
                                                class="form-control" name="terms_and_conditions" id="terms_and_conditions"
                                                value="{{ $generalSettings->terms_and_conditions }}">
                                            @error('terms_and_conditions')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    {{-- Language Settings --}}
                    <div id="tab-2" class="tab-pane">
                        <div class="panel-body">
                            <div class="row wrapper border-bottom white-bg page-heading" style="height: 9rem">
                                <div class="col-lg-7">
                                    <h2>Language Settings</h2>
                                    <ol class="breadcrumb">
                                        <li>
                                            <a href="/home">Home</a>
                                        </li>
                                        <li>
                                            <a href="/settings">Settings</a>
                                        </li>
                                        <li class="active">
                                            <strong>Language Settings</strong>
                                        </li>
                                    </ol>
                                </div>
                                <div class="col-lg-2">
                                    <a href="/translations" target="_blank" class="btn btn-warning mt-3">
                                        Translations</a>
                                </div>
                                <div class="col-lg-2">
                                    <a href="#exampleModal" data-toggle="modal" class="btn btn-primary mt-3">
                                        <span class="fa fa-plus"></span>
                                        Add Language</a>
                                </div>
                            </div>
                            <div class="wrapper wrapper-content animated fadeInRight">
                                <div class="modal fade" id="exampleModal" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h2 class="modal-title" id="exampleModalLabel">Fill Form</h2>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form method="post" class="form-horizontal"
                                                action="{{ route('languages.store') }}">
                                                <div class="modal-body">
                                                    @csrf
                                                    <div class="form-group"><label
                                                            class="col-sm-2 control-label">Name</label>
                                                        <div class="col-sm-10"><input type="text"
                                                                class="form-control" name="name"></div>
                                                    </div>
                                                    <div class="form-group"><label
                                                            class="col-sm-2 control-label">Code</label>
                                                        <div class="col-sm-10"><input type="text"
                                                                class="form-control" name="code"></div>
                                                    </div>
                                                    <div class="form-group"><label
                                                            class="col-sm-2 control-label">Select</label>
                                                        <div class="col-sm-10">
                                                            <select class="form-control" name="status" id="status">
                                                                <option>Select</option>
                                                                <option @if (old('status') == 'active') selected @endif value="active">Active
                                                                </option>
                                                                <option @if (old('status') == 'inactive') selected @endif value="inactive">Inactive
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Save</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Code</th>
                                            <th>Status</th>
                                            <th colspan="3">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($languages as $language)
                                            <tr>
                                                <td>{{ $language->name }}</td>
                                                <td>{{ $language->code }}</td>
                                                <td>{{ $language->status }}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a href="#" class="btn-white btn btn-xs" data-toggle="modal"
                                                            data-target="#editModal">Edit</a>
                                                        <div class="modal fade" id="editModal" tabindex="-1"
                                                            role="dialog" aria-labelledby="exampleModalLabel"
                                                            aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered"
                                                                role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h2 class="modal-title" id="exampleModalLabel">
                                                                            Edit language</h2>
                                                                        <button type="button" class="close"
                                                                            data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form method="post" class="form-horizontal"
                                                                        action="{{ route('languages.update', $language->id) }}">
                                                                        @csrf
                                                                        @method('PUT')
                                                                        <div class="modal-body">
                                                                            <div class="form-group"><label
                                                                                    class="col-sm-2 control-label">Name</label>
                                                                                <div class="col-sm-10"><input
                                                                                        type="text" class="form-control"
                                                                                        name="name"
                                                                                        value="{{ $language->name }}">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group"><label
                                                                                    class="col-sm-2 control-label">Code</label>
                                                                                <div class="col-sm-10"><input
                                                                                        type="text" class="form-control"
                                                                                        name="code"
                                                                                        value="{{ $language->code }}">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group"><label
                                                                                    class="col-sm-2 control-label">Select</label>
                                                                                <div class="col-sm-10">
                                                                                    <select class="form-control"
                                                                                        name="status" id="status">
                                                                                        <option>Select</option>
                                                                                        <option value="active"
                                                                                            {{ $language->status == 'active' ? 'selected' : '' }}>
                                                                                            Active</option>
                                                                                        <option value="inactive"
                                                                                            {{ $language->status == 'inactive' ? 'selected' : '' }}>
                                                                                            Inactive</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary"
                                                                                data-dismiss="modal">Close</button>
                                                                            <button type="submit"
                                                                                class="btn btn-primary">Submit</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <form method="post"
                                                            action="{{ route('languages.destroy', $language->id) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button
                                                                class="btn-white btn btn-xs delete-user">Delete</button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- SMTP Settings --}}
                    <div id="tab-3" class="tab-pane">
                        <div class="panel-body">
                            <div class="row wrapper border-bottom white-bg page-heading" style="height: 9rem">
                                <div class="col-lg-10">
                                    <h2>SMTP Settings</h2>
                                    <ol class="breadcrumb">
                                        <li>
                                            <a href="/home">Home</a>
                                        </li>
                                        <li>
                                            <a href="/settings">Settings</a>
                                        </li>
                                        <li class="active">
                                            <strong>SMTP Settings</strong>
                                        </li>
                                    </ol>
                                </div>
                                <div class="col-lg-1 mt-3">
                                    <button type="submit" class="btn btn-primary">
                                        <span class="fa fa-save"></span>
                                        Save</button>
                                </div>
                            </div>
                            <div class="wrapper wrapper-content animated fadeInRight">
                                <div class="row" style="margin:0px">
                                    <div class="col-sm-6">
                                        <div class="form-group"><label class="control-label">Id</label>
                                            <input type="text" placeholder="id" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group"><label class="control-label">Mailer</label>
                                            <input type="text" placeholder="mailer" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group"><label class="control-label">Host</label>
                                            <input type="text" placeholder="host" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group"><label class="control-label">Username</label>
                                            <input type="text" placeholder="username" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group"><label class="control-label">Password</label>
                                            <input type="text" placeholder="password" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group"><label class="control-label">Encryption</label>
                                            <input type="text" placeholder="encryption" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group"><label class="control-label">Sender Name</label>
                                            <input type="text" placeholder="sender_name" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group"><label class="control-label">Sender Email</label>
                                            <input type="text" placeholder="sender_email" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- SMS Settings --}}
                    <div id="tab-4" class="tab-pane">
                        <div class="panel-body">
                            <div class="row wrapper border-bottom white-bg page-heading" style="height: 9rem;">
                                <div class="col-lg-10">
                                    <h2>SMS Settings</h2>
                                    <ol class="breadcrumb">
                                        <li>
                                            <a href="/home">Home</a>
                                        </li>
                                        <li>
                                            <a href="/settings">Settings</a>
                                        </li>
                                        <li class="active">
                                            <strong>SMS Settings</strong>
                                        </li>
                                    </ol>
                                </div>
                                <div class="col-lg-1 mt-3">
                                    <button type="submit" class="btn btn-primary">
                                        <span class="fa fa-save"></span>
                                        Save</button>
                                </div>
                            </div>
                            <div class="wrapper wrapper-content animated fadeInRight">
                                <div class="row" id="show_hide" style="margin:0px;">
                                    <strong>SMS Credential Status</strong>
                                    <div>
                                        <input type="checkbox" value="true" class="js-switch-sms_1" />
                                    </div>
                                    <div class="sms">
                                        <div class="col-sm-6 mt-3">
                                            <div class="form-group"><label class="control-label">SMS Key</label>
                                                <input type="text" placeholder="Sms Key" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 mt-3">
                                            <div class="form-group"><label class="control-label">SMS Secret</label>
                                                <input type="text" placeholder="Sms Secret" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Addresses --}}
                    <div id="tab-5" class="tab-pane">
                        <div class="panel-body">
                            <div class="row wrapper border-bottom white-bg page-heading"
                                style="height: 9rem; padding-right:4rem;">
                                <div class="col-lg-10">
                                    <h2>Address Settings</h2>
                                    <ol class="breadcrumb">
                                        <li>
                                            <a href="/home">Home</a>
                                        </li>
                                        <li>
                                            <a href="/settings">Settings</a>
                                        </li>
                                        <li class="active">
                                            <strong>Address Settings</strong>
                                        </li>
                                    </ol>
                                </div>
                                <div class="col-lg-1">
                                    <a href="/create-address" class="btn btn-primary mt-3">
                                        <span class="fa fa-plus"></span>
                                        Add Address</a>
                                </div>
                            </div>
                            <div class="wrapper wrapper-content animated fadeInRight">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>Country</th>
                                            <th>State</th>
                                            <th>City</th>
                                            <th>Pin code</th>
                                            <th>Area</th>
                                            <th>Landmark</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>India</td>
                                            <td>Chhattisgarh</td>
                                            <td>Raipur</td>
                                            <td>492001</td>
                                            <td>Raipur</td>
                                            <td>Raipur</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="" class="btn-white btn btn-xs">View</a>

                                                    <a href="" class="btn-white btn btn-xs">Edit</a>

                                                    <a href="" class="btn-white btn btn-xs delete-user">Delete</a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- Social Links --}}
                    <div id="tab-6" class="tab-pane">
                        <form action="{{ route('social-links.update', $socialLinks->id) }}" method="POST"
                            class="panel-body">
                            <div class="row wrapper border-bottom white-bg page-heading" style="height: 9rem">
                                <div class="col-lg-10">
                                    <h2>Social Links</h2>
                                    <ol class="breadcrumb">
                                        <li>
                                            <a href="/home">Home</a>
                                        </li>
                                        <li>
                                            <a href="/settings">Settings</a>
                                        </li>
                                        <li class="active">
                                            <strong>Social Links</strong>
                                        </li>
                                    </ol>
                                </div>
                                <div class="col-lg-1 mt-3">
                                    <button type="submit" class="btn btn-primary">
                                        <span class="fa fa-save"></span>
                                        Save</button>
                                </div>
                            </div>
                            @csrf
                            @method('PUT')
                            <div class="wrapper wrapper-content animated fadeInRight">
                                <div class="row" style="margin:0px;">
                                    <div class="col-sm-6">
                                        <div class="form-group @error('facebook_url') has-error @enderror">
                                            <label for="facebook_url" class="control-label"> Facebook<span
                                                    class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter facebook link here..."
                                                class="form-control" name="facebook_url" id="facebook_url"
                                                value="{{ $socialLinks->facebook_url }}">
                                            @error('facebook_url')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group @error('twitter_url') has-error @enderror">
                                            <label for="twitter_url" class="control-label"> Twitter<span
                                                    class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter twitter link here..."
                                                class="form-control" name="twitter_url" id="twitter_url"
                                                value="{{ $socialLinks->twitter_url }}">
                                            @error('twitter_url')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group @error('pinterest_url') has-error @enderror">
                                            <label for="pinterest_url" class="control-label"> Pinterest<span
                                                    class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter pinterest link here..."
                                                class="form-control" name="pinterest_url" id="pinterest_url"
                                                value="{{ $socialLinks->pinterest_url }}">
                                            @error('pinterest_url')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group @error('youtube_url') has-error @enderror">
                                            <label for="youtube_url" class="control-label"> YouTube<span
                                                    class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter youtube link here..."
                                                class="form-control" name="youtube_url" id="youtube_url"
                                                value="{{ $socialLinks->youtube_url }}">
                                            @error('youtube_url')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group @error('linkedin_url') has-error @enderror">
                                            <label for="linkedin_url" class="control-label"> Linkedin<span
                                                    class="text-danger">*</span></label>
                                            <input type="text" placeholder="Enter linkedin link here..."
                                                class="form-control" name="linkedin_url" id="linkedin_url"
                                                value="{{ $socialLinks->linkedin_url }}">
                                            @error('linkedin_url')
                                                <span class="text-danger"> {{ $message }} </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    {{-- Payment Gateway Settings --}}
                    <div id="tab-7" class="tab-pane">
                        <div class="panel-body">
                            <div class="row wrapper border-bottom white-bg page-heading" style="height: 9rem;">
                                <div class="col-lg-10">
                                    <h2>Payment Settings</h2>
                                    <ol class="breadcrumb">
                                        <li>
                                            <a href="/home">Home</a>
                                        </li>
                                        <li>
                                            <a href="/settings">Settings</a>
                                        </li>
                                        <li class="active">
                                            <strong>Payment Settings</strong>
                                        </li>
                                    </ol>
                                </div>
                                <div class="col-lg-1 mt-3">
                                    <button type="submit" class="btn btn-primary">
                                        <span class="fa fa-save"></span>
                                        Save</button>
                                </div>
                            </div>
                            <div class="wrapper wrapper-content animated fadeInRight">
                                <div class="row" style="margin:0px; height:8rem">
                                    <strong>Stripe Credential status</strong>
                                    <div>
                                        <input type="checkbox" value="true" class="js-switch_1" />
                                    </div>
                                    <div class="stripe">
                                        <div class="col-sm-6 mt-3">
                                            <div class="form-group"><label class="control-label">Publishable
                                                    key</label>
                                                <input type="text" placeholder="Publishable key" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 mt-3">
                                            <div class="form-group"><label class="control-label">Secret key</label>
                                                <input type="text" placeholder="Secret key" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row" style="margin:0px;">
                                    <strong>RazorPay Credential status</strong>
                                    <div>
                                        <input type="checkbox" value="true" class="js-switch_2" />
                                    </div>
                                    <div class="razorpay">
                                        <div class="col-sm-6 mt-3">
                                            <div class="form-group"><label class="control-label">Publishable
                                                    key</label>
                                                <input type="text" placeholder="Publishable key" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 mt-3">
                                            <div class="form-group"><label class="control-label">Secret key</label>
                                                <input type="text" placeholder="Secret key" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    {{-- FAQs --}}
                    <div id="tab-8" class="tab-pane">
                        <div class="panel-body">
                            <div class="row wrapper border-bottom white-bg page-heading"
                                style="height: 9rem; padding-right:4rem;">
                                <div class="col-lg-10">
                                    <h2>FAQs Settings</h2>
                                    <ol class="breadcrumb">
                                        <li>
                                            <a href="/home">Home</a>
                                        </li>
                                        <li>
                                            <a href="/settings">Settings</a>
                                        </li>
                                        <li class="active">
                                            <strong>FAQs Settings</strong>
                                        </li>
                                    </ol>
                                </div>
                                <div class="col-lg-1">
                                    <a href="{{ route('faqs.create') }}" class="btn btn-primary mt-3">
                                        <span class="fa fa-plus"></span>
                                        Add FAQ</a>
                                </div>
                            </div>
                            <div class="wrapper wrapper-content animated fadeInRight">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Question</th>
                                            <th>Answer</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($faqs as $faq)
                                            <tr>
                                                <td>{{ $faq->title }}</td>
                                                <td>{{ $faq->description }}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a href="{{ route('faqs.show', $faq->id) }}"
                                                            class="btn-white btn btn-xs">View</a>

                                                        <a href="{{ route('faqs.edit', $faq->id) }}"
                                                            class="btn-white btn btn-xs">Edit</a>

                                                        <a href="javascript:;" data-faq-id="{{ $faq->id }}"
                                                            class="btn-white btn btn-xs delete-faq">Delete</a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"
        integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew=="
        crossorigin="anonymous" referrerpolicy="no-referrer">
    </script>

    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous">
    </script>

    <script>
        $('.dropify').dropify();
    </script>

    <!-- Mainly scripts -->
    <script src="{{ asset('asset/js/jquery-2.1.1.js') }}"></script>
    <script src="{{ asset('asset/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('asset/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>

    <!-- Switchery -->
    <script src="{{ asset('asset/js/plugins/switchery/switchery.js') }}"></script>

    <!-- SUMMERNOTE -->
    <script src="{{ asset('asset/js/plugins/summernote/summernote.min.js') }}"></script>

    <script>
        $(document).ready(function() {

            // Stripe payment gateway
            var elem_1 = document.querySelector('.js-switch_1');
            var switchery_1 = new Switchery(elem_1, {
                color: '#1AB394'
            });

            $('.js-switch_1').on('change', function() {
                this.value = this.checked ? 1 : 0;
                $('.stripe').toggle();
            }).change();

            // Razorpay payment gateway
            var elem_2 = document.querySelector('.js-switch_2');
            var switchery_2 = new Switchery(elem_2, {
                color: '#1AB394'
            });

            $('.js-switch_2').on('change', function() {
                this.value = this.checked ? 1 : 0;
                $('.razorpay').toggle();
            }).change();

            // SMS Credential
            var elem_3 = document.querySelector('.js-switch-sms_1');
            var switchery_3 = new Switchery(elem_3, {
                color: '#1AB394'
            });

            $('.js-switch-sms_1').on('change', function() {
                this.value = this.checked ? 1 : 0;
                $('.sms').toggle();
            }).change();

            $('.summernote').summernote();

            $('.delete-faq').click(function() {
                let faqId = $(this).data('faq-id');
                let token = '{{ csrf_token() }}';

                let url = '{{ route('faqs.destroy', ':id') }}';
                url = url.replace(':id', faqId);

                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function() {
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {
                            "_token": token,
                            "_method": 'DELETE',
                        },
                        success: function() {
                            swal("Deleted!", "User account has been deleted.",
                                "success");
                            setTimeout(() => {
                                location.reload();
                            }, 3000);
                        }
                    });
                });
            });
        });

        var edit = function() {
            $('.click2edit').summernote({
                focus: true
            });
        };

        var save = function() {
            var aHTML = $('.click2edit').code();
            $('.click2edit').destroy();
        };
    </script>
@endsection
