@extends('layouts.dashboard')

@section('title')
    Show Coupon
@endsection
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"
        integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Show Coupon</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/home">Home</a>
                </li>
                <li>
                    <a href="{{ route('coupons.index') }}">Coupon</a>
                </li>
                <li class="active">
                    <strong>Show Coupon</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <a href="{{ route('coupons.index') }}" class="btn btn-warning mt-3" id="save-data" style="margin-top: 3rem">
                <span class="fa fa-arrow-left"></span>
                Back
            </a>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <form action="{{ route('coupons.show', $coupons->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-lg-9">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Coupons Details</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group @error('title') has-error @enderror">
                                        <label for="title">Title <span class="text-danger">*</span></label>
                                        <input type="text" placeholder="Enter title" class="form-control" title="title"
                                            id="title" value="{{ $coupons->title }}">
                                        @error('title')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('code') has-error @enderror">
                                        <label for="code">Coupon code<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter code" name="code"
                                            id="code" value="{{ $coupons->code }}">
                                        @error('code')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('discount') has-error @enderror">
                                        <label for="discount">Discount<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter discount"
                                            name="discount" id="discount" value="{{ $coupons->discount }}">
                                        @error('discount')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('description') has-error @enderror">
                                        <label for="description">Description<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter description"
                                            name="description" id="description" value="{{ $coupons->description }}">
                                        @error('discription')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@section('script')
    <script>
        $(document).on('click', '#save-data', function() {
            $('form').submit();
        });
    </script>
@endsection
@endsection
