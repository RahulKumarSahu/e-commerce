@extends('layouts.dashboard')

@section('title')
    Coupons
@endsection
@section('css')
    <link href="{{ asset('asset/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-11">
            <h2>Coupon</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/home">Home</a>
                </li>
                <li class="active">
                    <strong>Coupon</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-1">
            <a href="{{ route('coupons.create') }}" class="btn btn-primary mt-3">
                <span class="fa fa-plus"></span>
                Add</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Coupon List
                        </h5>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover coupon-table">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Coupon Code</th>
                                        <th>Discount</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($coupons as $coupon)
                                        <tr>
                                            <td>{{ $coupon->title }}</td>
                                            <td>{{ $coupon->code }}</td>
                                            <td>{{ $coupon->discount }}</td>
                                            <td>{{ $coupon->description }}</td>
                                            <td>
                                                @if ($coupon->status == 'active')
                                                    <span class="label label-primary">Active</span>
                                                @else
                                                    <span class="label label-danger">In-Active</span>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('coupons.show', $coupon->id) }}"
                                                        class="btn-white btn btn-xs">View</a>

                                                    <a href="{{ route('coupons.edit', $coupon->id) }}"
                                                        class="btn-white btn btn-xs">Edit</a>

                                                    <a href="javascript:;" data-coupon-id="{{ $coupon->id }}"
                                                        class="btn-white btn btn-xs delete-coupon">Delete</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    {{-- Datatable --}}
    <script src=" {{ asset('asset/js/plugins/dataTables/datatables.min.js') }}"></script>
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {
            $('.coupon-table').DataTable();

            @if (session('success'))
                toastr.success('{{ session('success') }}', 'Success');
            @endif

            $('.delete-coupon').click(function() {
                let couponId = $(this).data('coupon-id');
                let token = '{{ csrf_token() }}';

                let url = '{{ route('coupons.destroy', ':id') }}';
                url = url.replace(':id', couponId);

                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function() {
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {
                            "_token": token,
                            "_method": 'DELETE',
                        },
                        success: function() {
                            swal("Deleted!", "User account has been deleted.",
                                "success");
                            setTimeout(() => {
                                location.reload();
                            }, 3000);
                        }
                    });
                });
            });
        });
    </script>
@endsection
