@extends('layouts.dashboard')

@section('title')
    Coupon
@endsection
@section('css')
    <link href="{{ asset('asset/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('asset/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
@endsection
@section('breadcrum')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Add Coupon</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/home">Home</a>
                </li>
                <li>
                    <a href="{{ route('coupons.index') }}">Coupon</a>
                </li>
                <li class="active">
                    <strong>Add Coupon</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <a href="{{ route('coupons.index') }}" class="btn btn-warning mt-3" id="save-data">
                <span class="fa fa-arrow-left"></span>
                Back
            </a>
            <button class="btn btn-primary mt-3" id="save-data"> <span class="fa fa-save"></span> Save</button>
        </div>
    </div>
@endsection
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <form action="{{ route('coupons.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3>Coupon Details</h3>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group @error('title') has-error @enderror">
                                        <label for="title">Title <span class="text-danger">*</span></label>
                                        <input type="text" placeholder="Enter title" class="form-control" name="title"
                                            id="title" value="{{ old('title') }}">
                                        @error('title')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('code') has-error @enderror">
                                        <label for="code">Coupon Code <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter code" name="code"
                                            id="code" value="{{ old('code') }}">
                                        @error('password')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('discount') has-error @enderror">
                                        <label for="discount">Discount <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter discount"
                                            name="discount" id="discount" value="{{ old('discount') }}">
                                        @error('discount')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('description') has-error @enderror">
                                        <label for="description">Description <span class="text-danger">*</span></label>
                                        <textarea class="form-control summernote" name="description" id="description">
                                                                </textarea>
                                        @error('description')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group @error('status') has-error @enderror">
                                        <label for="status">Status <span class="text-danger">*</span></label>
                                        <select class="form-control" name="status" id="state">
                                            <option selected disabled>Select</option>
                                            <option value="active">active</option>
                                            <option value="inactive">inactive</option>
                                        </select>
                                        @error('status')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <!-- Mainly scripts -->
    <script src="{{ asset('js/dashboard.js') }}"></script>
    <script src="{{ asset('asset/js/jquery-2.1.1.js') }}"></script>
    <script src="{{ asset('asset/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('asset/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('asset/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- Custom and plugin javascript -->
    <script src="{{ asset('asset/js/inspinia.js ') }}"></script>
    <script src="{{ asset('asset/js/plugins/pace/pace.min.js ') }}"></script>
    <!-- SUMMERNOTE -->
    <script src="{{ asset('asset/js/plugins/summernote/summernote.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.summernote').summernote();

        });
        $(document).on('click', '#save-data', function() {
            $('form').submit();
        });
    </script>
@endsection
