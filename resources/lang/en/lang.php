<?php

return array (
  'Dashboard' => 'Dashboard',
  'Users' => 'Users',
  'Manage Products' => 'Manage Products',
  'Category' => 'Category',
  'Sub Category' => 'Sub Category',
  'Products' => 'Products',
  'Coupons' => 'Coupons',
  'Manage Orders' => 'Manage Orders',
  'Orders' => 'Orders',
  'Invoices' => 'Invoices',
  'Reports' => 'Reports',
  'Settings' => 'Settings',
);
