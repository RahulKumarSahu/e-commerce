<?php

return array (
    'Dashboard' => 'डैशबोर्ड',
    'Users' => 'उपयोगकर्ताओं',
    'Manage Products' => 'उत्पादों का प्रबंधन करें',
    'Category' => 'श्रेणी',
    'Sub Category' => 'उप श्रेणी',
    'Products' => 'उत्पादों',
    'Coupons' => 'कूपन',
    'Manage Orders' => 'आदेश प्रबंधित करें',
    'Orders' => 'आदेश',
    'Invoices' => 'चालान',
    'Reports' => 'रिपोर्टों',
    'Settings' => 'समायोजन',
);
