<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreGeneralSettings extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'site_name' => 'required',
            'language' => 'required',
            'email' => 'required|max:60',
            'site_logo' => 'required',
            'favicon_logo' => 'required',
            'title' => 'required',
            'address' => 'required',
            'contact1' => 'required',
            'terms_and_conditions' => 'required',
        ];

        // $rules = [];
        // $rules['site_name'] = 'required';
        // $rules['language'] = 'required';
        // $rules['email'] = 'required|max:60';
        // $rules['favicon_logo'] = 'required|unique:categories,slug';
        // $rules['site_logo'] = 'required';
        // $rules['title'] = 'required';
        // $rules['address'] = 'required';
        // $rules['contact1'] = 'required';
        // $rules['terms_and_conditions'] = 'required';

        // return $rules;
    }
}
