<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use App\Models\GeneralSetting;
use App\Models\LanguageSetting;
use App\Models\SocialLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SettingController extends Controller
{
    public function index(){
       $code =  GeneralSetting::where('id', 1)->value('language');
        Session::put('locale',$code);
        app()->setLocale(Session::get('locale'));
        $generalSettings = GeneralSetting::first();
        $socialLinks = SocialLink::first();
        $faqs = Faq::get();
        $languages = LanguageSetting::get();
        return view('dashboard.settings.index', compact('generalSettings', 'socialLinks', 'faqs', 'languages'));
    }
}
