<?php

namespace App\Http\Controllers;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductDetailController extends Controller
{
    public function index()
    {
        $products = Product::get();
        return view('front.detail', compact('products'));
    }

    public function show($slug)
    {
        $products = Product::where('slug', $slug)->first();
        return view('front.detail', compact('products'));
    }
}

