<?php

namespace App\Http\Controllers;

use App\Models\LanguageSetting;
use Illuminate\Http\Request;
use JetBrains\PhpStorm\Language;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class LanguageSettingController extends Controller
{
    public function index()
    {
        $languages = LanguageSetting::get();
        return view('dashboard.settings.index', compact('languages'));
    }

    public function create()
    {
        return view('dashboard.languages.create');
    }

    public function store(Request $request)
    {
        $languages = new LanguageSetting;
        $languages->name = $request->name;
        $languages->code = $request->code;
        $languages->status = $request->status;
        $path = base_path() . '/resources/lang/' . $request->code;
        if (!file_exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }
        $languages->save();
        return redirect('/settings');
    }

    public function edit($id)
    {
        $languages = LanguageSetting::find($id);
        return view('dashboard.languages.edit', compact('languages'));
    }

    public function update(Request $request, $id)
    {
        $languages = LanguageSetting::find($id);
        $languages->name = $request->name;
        $languages->code = $request->code;
        $languages->status = $request->status;
        $languages->save();
        return redirect('/settings');

    }
    
    public function destroy($id)
    {
        $code = LanguageSetting::select('code')->where('id',$id )->pluck('code');
        if($code[0] =='en'){
            return redirect('/settings');
        }
        $path = base_path().'/resources/lang/'.$code[0];
        if (file_exists($path)) {
              File::deleteDirectory($path);
          }
        LanguageSetting::find($id)->delete();
        return redirect('/settings');

    }
}
