<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{

    public function index()
    {
        $faqs = Faq::get();
        return view('dashboard.settings.index', compact('faqs'));
    }

    public function create()
    {
        return view('dashboard.faqs.create');
    }

    public function store(Request $request)
    {
        $faqs = new Faq;
        $faqs->title = $request->title;
        $faqs->description = $request->description;
        $faqs->status = $request->status;
        $faqs->save();
        return redirect('/settings');
    }

    public function show($id)
    {
        $faqs = Faq::find($id);
        return view('dashboard.faqs.show', compact('faqs'));
    }

    public function edit($id)
    {
        $faqs = Faq::find($id);
        return view('dashboard.faqs.edit', compact('faqs'));
    }

    public function update(Request $request,$id)
    {
        $faqs = Faq::find($id);
        $faqs->title = $request->title;
        $faqs->description = $request->description;
        $faqs->status = $request->status;
        $faqs->save();
        return redirect('/settings');
    }

    public function destroy($id)
    {
        Faq::find($id)->delete();
        return redirect('/settings');
    }
}
