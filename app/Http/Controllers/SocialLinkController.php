<?php

namespace App\Http\Controllers;

use App\Models\SocialLink;
use Illuminate\Http\Request;

class SocialLinkController extends Controller
{

    public function index()
    {
        $socialLinks = SocialLink::first();
        return view('dashboard.settings.index', compact('socialLinks'));
    }

    public function create()
    {
        return view('dashboard.settings.index');
    }

    public function edit($id)
    {
        $socialLink = SocialLink::find($id);
        return view('dashboard.settings.index', compact('socialLink'));
    }

    public function update(Request $request, $id)
    {
        $socialLinks = SocialLink::find($id);
        $socialLinks->facebook_url = $request->facebook_url;
        $socialLinks->twitter_url = $request->twitter_url;
        $socialLinks->pinterest_url = $request->pinterest_url;
        $socialLinks->youtube_url = $request->youtube_url;
        $socialLinks->linkedin_url = $request->linkedin_url;
        $socialLinks->save();

        return redirect('/settings');
    }

}
