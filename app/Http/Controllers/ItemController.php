<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\SubCategory;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function index()
    {

        $categories = Category::get();
        $subcategories = SubCategory::get();
        $products = Product::get();
        return view('front.items', compact('categories', 'subcategories', 'products'));
    }
    public function show($id)
    {

        $categories = Category::get();
        $subcategories = SubCategory::get();
        $products = Product::find($id);
        return view('front.items', compact('categories', 'subcategories', 'products'));
    }
}