<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\StoreProduct;
use App\Http\Requests\Product\UpdateProduct;
use App\Models\Category;
use App\Models\Product;
use App\Models\SubCategory;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::get();
        return view('dashboard.products.index', compact('products'));
        // return $products = Product::get()->toJson();
    }

    public function create()
    {
        $categories = Category::get();
        $subCategories = SubCategory::get();

        return view('dashboard.products.create', compact('categories', 'subCategories'));
    }

    public function store(StoreProduct $request)
    {
        $products = new Product;
        $products->category_id = $request->category_id;
        $products->sub_category_id = $request->sub_category_id;
        $products->name = $request->name;
        $products->price = $request->price;
        $products->quantity = $request->quantity;
        $products->featured = $request->featured;
        $products->description = $request->description;
        $products->status = $request->status;
        $products->save();

        // if ($request->hasfile('File')) {
        //     foreach ($request->file('File') as $file) {
        //         $name = $file->getClientOriginalName();
        //         $file->move(public_path() . '/uploads/', $name);
        //         $imgData[] = $name;
        //     }
        // }
        return redirect('products')->with(['success' => 'Product added.']);
    }

    public function show($id)
    {
        $products = Product::find($id);
        $categories = Category::get();
        $subCategories = SubCategory::get();

        return view('dashboard.products.show', compact('products', 'categories', 'subCategories'));
    }

    public function edit($id)
    {
        $products = Product::find($id);
        $categories = Category::get();
        $subCategories = SubCategory::get();

        return view('dashboard.products.edit', compact('products', 'categories', 'subCategories'));
    }

    public function update(UpdateProduct $request, $id)
    {
        $products = Product::find($id);
        $products->name = $request->name;
        $products->price = $request->price;
        $products->quantity = $request->quantity;
        $products->featured = $request->featured;
        $products->description = $request->description;
        $products->status = $request->status;
        $products->save();

        return redirect('/products')->with(['success' => 'Product has been updated.']);
    }

    public function destroy($id)
    {
        Product::find($id)->delete();
        return redirect('/products')->with(['success' => 'Product has been deleted.']);
    }
}