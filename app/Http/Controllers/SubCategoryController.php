<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubCategory\StoreSubCategory;
use App\Http\Requests\SubCategory\UpdateSubCategory;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\SubCategory;

class SubCategoryController extends Controller
{
    public function index()
    {
        $subCategories = SubCategory::get();
        return view('dashboard.sub-categories.index', compact('subCategories'));
    }

    public function create()
    {
        $categories = Category::get();
        return view('dashboard.sub-categories.create', compact('categories'));
    }

    public function store(StoreSubCategory $request)
    {
        $subCategories = new SubCategory;
        $subCategories->category_id = $request->category_id;
        $subCategories->name = $request->name;
        $subCategories->status = $request->status;
        $subCategories->featured = $request->featured;
        $subCategories->save();

        return redirect()->route('sub-categories.index')->with(['success' => 'SubCategory has been created.']);
    }

    public function show($id)
    {
        $subCategories = SubCategory::find($id);

        $categories = Category::get();

        return view('dashboard.sub-categories.show', compact('subCategories', 'categories'));
    }

    public function edit($id)
    {
        $subCategories = SubCategory::find($id);

        $categories = Category::get();
        return view('dashboard.sub-categories.edit', compact('subCategories', 'categories'));
    }

    public function update(Request $request, $id)
    {
        $subCategories = SubCategory::find($id);
        $subCategories->category_id = $request->category_id;
        $subCategories->name = $request->name;
        $subCategories->status = $request->status;
        $subCategories->featured = $request->featured;
        $subCategories->save();
        return redirect()->route('sub-categories.index')->with(['success' => 'SubCategory has been updated.']);
    }

    public function destroy($id)
    {
        SubCategory::find($id)->delete();
        return redirect()->route('sub-categories.index')->with(['success' => 'SubCategory has been deleted.']);
    }

    public function getSubcategories($categoryId)
    {
        $subCategories = SubCategory::where('category_id', $categoryId)->get();

        $options = '<option>--select--</option>';

        foreach ($subCategories as $subCategory) {
            $options .= '<option value="' . $subCategory->id . '">' . $subCategory->name . '</option>';
        }

        // return Response($options);
        return $options;
    }
}