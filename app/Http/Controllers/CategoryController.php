<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\StoreCategory;
use App\Http\Requests\Category\UpdateCategory;
use App\Models\Category;


class categoryController extends Controller
{
    public function index()
    {
        $categories = Category::get();
        return view('dashboard.categories.index', compact('categories'));
    }

    public function create()
    {
        return view('dashboard.categories.create');
    }

    public function store(StoreCategory $request)
    {
        $categories = new Category;
        $categories->name = $request->name;
        $categories->status = $request->status;
        $categories->featured = $request->featured;
        $categories->save();

        return redirect()->route('categories.index')->with(['success' => 'category has been created.']);
    }

    public function show($id)
    {
        $categories = Category::find($id);
        return view('dashboard.categories.show', compact('categories'));
    }

    public function edit($id)
    {
        $categories = Category::find($id);
        return view('dashboard.categories.edit', compact('categories'));
    }

    public function update(UpdateCategory $request, $id)
    {
        $category = Category::find($id);
        $category->name = $request->name;
        $category->status = $request->status;
        $category->featured = $request->featured;
        $category->save();
        return redirect()->route('categories.index')->with(['success' => 'category has been updated.']);
    }

    public function destroy($id)
    {
        Category::find($id)->delete();
        return redirect('/categories')->with(['success' => 'category has been deleted.']);
    }
}