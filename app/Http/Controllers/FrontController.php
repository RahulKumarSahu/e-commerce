<?php

namespace App\Http\Controllers;
use App\Models\Front;
use App\Models\Category;
use App\Models\Faq;
use App\Models\GeneralSetting;
use App\Models\Product;
use App\Models\SocialLink;
use App\Models\SubCategory;
class FrontController extends Controller
{
    public function index()
    {
        $categories = Category::get();
        $subcategories = SubCategory::get();
        $products = Product::get();
        $generalSettings = GeneralSetting::first();
        $socialLinks = SocialLink::first();
        return view('front.home', compact('categories', 'subcategories', 'products', 'generalSettings', 'socialLinks'));
    }

    public function productDetail($slug)
    {
            $products = Product::where('slug', $slug)->first();
            $socialLinks = SocialLink::first();
            return view('front.detail', compact('products', 'socialLinks'));
    }

    public function cart()
    {
        $socialLinks = SocialLink::first();
        return view('front.cart', compact('socialLinks'));
    }

    public function faq()
    {

        $faqs = Faq::get();
        return view('front.faq', compact('faqs'));
    }

    public function contact()
    {

        $generalSettings = GeneralSetting::first();
        return view('front.contact', compact('generalSettings'));
    }
    
    public function signIn(){

        $socialLinks = SocialLink::first();
        return view('front.sign-in', compact('socialLinks'));
    }

    public function signUp(){

        $socialLinks = SocialLink::first();
        return view('front.sign-up', compact('socialLinks'));
    }

    public function forgotPassword(){

        $socialLinks = SocialLink::first();
        return view('front.forgot-password', compact('socialLinks'));
    }

    public function resetPassword(){

        $socialLinks = SocialLink::first();
        return view('front.reset-password', compact('socialLinks'));
    }
}