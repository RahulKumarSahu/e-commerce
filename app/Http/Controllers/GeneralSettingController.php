<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreGeneralSettings;
use App\Models\GeneralSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class GeneralSettingController extends Controller
{
    public function index()
    {
        $generalSettings = GeneralSetting::first();
        return view('dashboard.settings.index', compact('generalSettings'));
    }

    public function edit($id)
    {
        $generalSetting = GeneralSetting::find($id);
        return view('dashboard.settings.index', compact('generalSetting'));
    }

    public function update(Request $request, $id)
    {
        $generalSettings = GeneralSetting::find($id);
        $generalSettings->site_name = $request->site_name;
        $generalSettings->language = $request->language;
        $generalSettings->title = $request->title;
        $generalSettings->address = $request->address;
        $generalSettings->contact1 = $request->contact1;
        $generalSettings->contact2 = $request->contact2;
        $generalSettings->email = $request->email;
        $generalSettings->terms_and_conditions = $request->terms_and_conditions;
        $generalSettings->save();
        return redirect('/settings');
    }

}
