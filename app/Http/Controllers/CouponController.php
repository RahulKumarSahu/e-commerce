<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCouponRequest;
use App\Models\Coupon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    public function index()
    {
        $coupons = Coupon::get();
        return view('dashboard.coupons.index', compact('coupons'));
    }

    public function create()
    {
        return view('dashboard.coupons.create');
    }

    public function store(StoreCouponRequest $request)
    {
        $coupons = new Coupon;
        $coupons->title = $request->title;
        $coupons->code = $request->code;
        $coupons->discount = $request->discount;
        $coupons->description = $request->description;
        $coupons->status = $request->status;
        $coupons->save();

        return redirect('/coupons')->with(['success' => 'Coupons has been created.']);
    }

    public function show($id)
    {
        $coupons = Coupon::find($id);
        return view('dashboard.coupons.show', compact('coupons'));
    }

    public function edit($id)
    {
        $coupons = Coupon::find($id);
        return view('dashboard.coupons.edit', compact('coupons'));
    }

    public function update(Request $request, $id)
    {
        $coupons = Coupon::find($id);
        $coupons->title = $request->title;
        $coupons->code = $request->code;
        $coupons->discount = $request->discount;
        $coupons->description = $request->description;
        $coupons->status = $request->status;
        $coupons->save();
        return redirect('/coupons')->with(['success' => 'Coupons has been updated.']);
    }

    public function destroy($id)
    {
        Coupon::find($id)->delete();
        return redirect('/coupons')->with(['success' => 'Coupons has been deleted.']);
    }
}