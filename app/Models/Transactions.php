<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    use HasFactory;
      protected $fillable = [
     'order_id',
    'mode',
    'amount',
    'status',
    'created_by',
    'updated_by',
     ];
}
