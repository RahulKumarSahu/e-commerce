<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentSetting extends Model
{
    use HasFactory;
    protected $fillable = [
    'paypal_key',
    'paypal_secret',
    'paypal_status',
     'razorpay_key',
    'razorpay_secret',
    'razorpay_status',
     'stripe_key',
    'stripe_secret',
    'stripe_status',
    'created_by',
    'updated_by',
     ];
}
