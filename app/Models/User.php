<?php

namespace App\Models;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Observers\UserObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;
    protected static function boot()
    {
        parent::boot();
        static::observe(UserObserver::class);
    }
    // Accessor
    public function getProfilePhotoAttribute()
    {
        $defaultPath = 'https://www.kindpng.com/picc/m/65-653274_workers-compensation-law-social-security-disability-user-icon.png';

        $path = asset('user-uploads/users/' . $this->photo);

        return $this->photo == '' ? $defaultPath : $path;
    }

}
