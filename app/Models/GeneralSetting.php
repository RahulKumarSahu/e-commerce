<?php

namespace App\Models;

use App\Observers\GeneralSettingsObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class GeneralSetting extends Model
{
    use HasFactory, Notifiable;

    protected static function boot()
    {
        parent::boot();
        static::observe(GeneralSettingsObserver::class);
    }

    protected $fillable = [
        'site_name',
        'language ',
        'site_logo',
        'favicon_logo',
        'title',
        'address',
        'contact1 ',
        'contact2 ',
        'email',
        'terms_and_conditions',
        'created_by',
        'updated_by',
    ];
}
