<?php

namespace App\Models;

use App\Observers\SubCategoryObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    use HasFactory;
    protected $fillable = [
        'category_id',
        'name',
        'slug',
        'photo',
        'tags',
        'status',
        'created_by',
        'updated_by',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    protected static function boot()
    {
        parent::boot();
        static::observe(SubCategoryObserver::class);
    }
    // Accessor
    public function getSubCategoryPhotoAttribute()
    {
        $defaultPath = asset('products-uploads/defualt/not found.png');

        $path = asset('sub-categories-uploads/sub-categories/' . $this->photo);

        return $this->photo == '' ? $defaultPath : $path;
    }
}