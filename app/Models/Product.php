<?php

namespace App\Models;

use App\Observers\ProductsObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected static function boot()
    {
        parent::boot();
        static::observe(ProductsObserver::class);
    }


    protected $fillable = [
        'name',
        'price',
        'descriptions',
        'photo',
        'quantity',
        'status',
        'created_by',
        'updated_by',
    ];

    // Accessor
    public function getProductPhotoAttribute()
    {
        $defaultPath = asset('products-uploads/defualt/not found.png');

        $path = asset('products-uploads/products/' . $this->photo);

        return $this->photo == '' ? $defaultPath : $path;
    }
}