<?php

namespace App\Models;

use App\Observers\CategoryObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        
        'name',
        'photo',
        'status',
        'created_by',
        'updated_by',
    ];

    public function subcategory(){
        return $this ->hasMany(SubCategory::class);
    }

    protected static function boot()
    {
        parent::boot();
        static::observe(CategoryObserver::class);
    }
    // Accessor
    public function getCategoryPhotoAttribute()
    {
        $defaultPath = asset('products-uploads/defualt/not found.png');

        $path = asset('categories-uploads/categories/' . $this->photo);

        return $this->photo == '' ? $defaultPath : $path;
    }
}