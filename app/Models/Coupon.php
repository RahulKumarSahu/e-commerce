<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;
    protected $fillable = [

    'title',
    'code',
    'description',
    'discount',
    'status',
    'created_by',
    'updated_by',
 ];
}
