<?php

namespace App\Observers;

use App\Models\User;
use Illuminate\Support\Facades\File;

class UserObserver
{
    public function creating(User $user)
    {
        if (request()->file) {
            $file_name = time() . '.' . request()->file->extension();
            $user->photo = $file_name; // Save file name to database
        }
    }


    public function created(User $user)
    {
        if (request()->file) {
            $path = public_path('user-uploads/users');
            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $user->photo);
        }
    }

    public function saving(User $user)
    {
        if (request()->file) {

            // Old file delete code
            $path = public_path('user-uploads/users/');
            $this->deleteFile($path . $user->photo);

            $file_name = time() . '.' . request()->file->extension();
            $user->photo = $file_name; // Save file name to database
        }
    }


    public function updated(User $user)
    {
        if (request()->file) {

            $path = public_path('user-uploads/users/');

            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $user->photo);
        }
    }

    public function deleted(User $user)
    {
        $path = public_path('user-uploads/users/');

        // Old file delete code
        $this->deleteFile($path . $user->photo);
    }

    public function restored(User $user)
    {
        //
    }


    public function forceDeleted(User $user)
    {
        //
    }

    public function deleteFile($path)
    {
        File::delete($path);
    }
}