<?php

namespace App\Observers;

use App\Models\SubCategory;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class SubCategoryObserver
{
    public function creating(SubCategory $subCategories)
    {
        if (request()->file) {
            $file_name = time() . '.' . request()->file->extension();
            $subCategories->photo = $file_name; // Save file name to database
        }

        $subCategories->slug = Str::slug($subCategories->name, '-');
    }

    public function created(SubCategory $subCategories)
    {
        if (request()->file) {
            $path = public_path('sub-categories-uploads/sub-categories');
            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $subCategories->photo);
        }
    }

    public function saving(SubCategory $subCategories)
    {
        if (request()->file) {

            // Old file delete code
            $path = public_path('sub-categories-uploads/sub-categories/');
            $this->deleteFile($path . $subCategories->photo);

            $file_name = time() . '.' . request()->file->extension();
            $subCategories->photo = $file_name; // Save file name to database
        }

        $subCategories->slug = Str::slug($subCategories->name, '-');
    }

    public function updated(SubCategory $subCategories)
    {
        if (request()->file) {

            $path = public_path('sub-categories-uploads/sub-categories/');

            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $subCategories->photo);
        }
    }

    public function deleted(SubCategory $subCategories)
    {
        $path = public_path('sub-categories-uploads/sub-categories/');

        // Old file delete code
        $this->deleteFile($path . $subCategories->photo);
    }

    public function deleteFile($path)
    {
        File::delete($path);
    }
}