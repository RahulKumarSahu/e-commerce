<?php

namespace App\Observers;

use App\Models\Category;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;


class CategoryObserver
{
    public function creating(Category $categories)
    {
        if (request()->file) {
            $file_name = time() . '.' . request()->file->extension();
            $categories->photo = $file_name; // Save file name to database
        }
        $categories->slug = Str::slug($categories->name, '-');
    }
    public function created(Category $categories)
    {
        if (request()->file) {
            $path = public_path('categories-uploads/categories');
            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $categories->photo);
        }
    }

    public function saving(Category $categories)
    {
        if (request()->file) {

            // Old file delete code
            $path = public_path('categories-uploads/categories/');
            $this->deleteFile($path . $categories->photo);

            $file_name = time() . '.' . request()->file->extension();
            $categories->photo = $file_name; // Save file name to database
        }
        $categories->slug = Str::slug($categories->name, '-');
    }

    public function updated(Category $categories)
    {
        if (request()->file) {

            $path = public_path('categories-uploads/categories/');

            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $categories->photo);
        }
    }

    public function deleted(Category $categories)
    {
        $path = public_path('categories-uploads/categories/');

        // Old file delete code
        $this->deleteFile($path . $categories->photo);
    }

    public function deleteFile($path)
    {
        File::delete($path);
    }
}