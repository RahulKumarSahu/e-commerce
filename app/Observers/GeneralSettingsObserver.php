<?php

namespace App\Observers;

use App\Models\GeneralSetting;
use Illuminate\Support\Facades\File;


class GeneralSettingsObserver
{

    public function saving(GeneralSetting $GeneralSetting)
    {

        if (request()->site_logo) {

            // Old file delete code
            $path = public_path('GeneralSetting-uploads/GeneralSetting/');
            $this->deleteFile($path . $GeneralSetting->site_logo);

            $file_name = request()->site_logo->getClientOriginalName();
            $GeneralSetting->site_logo  = $file_name; // Save file name to database

        }
        if (request()->favicon_logo) {

            // Old file delete code
            $path = public_path('GeneralSetting-uploads/GeneralSetting/');
            $this->deleteFile($path . $GeneralSetting->favicon_logo);

            $file_name =  request()->favicon_logo->getClientOriginalName();
            $GeneralSetting->favicon_logo  = $file_name; // Save file name to database

        }
    }

    public function saved(GeneralSetting $GeneralSetting)
    {
        if (request()->site_logo) {

            $path = public_path('GeneralSetting-uploads/GeneralSetting/');

            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            $name  =  request()->site_logo->move($path, $GeneralSetting->site_logo);
        }
        if (request()->favicon_logo) {

            $path = public_path('GeneralSetting-uploads/GeneralSetting/');

            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            $name =  request()->favicon_logo->move($path, $GeneralSetting->favicon_logo);
        }
    }

    public function deleted(GeneralSetting $GeneralSetting)
    {
        $path = public_path('GeneralSetting-uploads/GeneralSetting/');

        // Old file delete code
        $this->deleteFile($path . $GeneralSetting->site_logo);
        $this->deleteFile($path . $GeneralSetting->favicon_logo);
    }

    public function deleteFile($path)
    {
        File::delete($path);
    }
}
